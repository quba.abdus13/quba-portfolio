// import Desc from "../../components/desc/desc";
// import Icon from "../../components/icon/icon";
// import Title from "../../components/title/title";
// import Marquee from "../../components/marquee/marquee";
// import Button from "../../components/button/button";
// import headerStyle from "../../components/header/header.module.scss";
// import ImgOverlay from "../../components/imgOverlay/imgOverlay";
// import TransitionCard from "../../components/transitionCard/transitionCard";
// import DisclosureX from "../../components/customDisc/customDisc";
// import { getImageUrl } from "../../utils/images";
// import PopUpCard from "../../components/popUpCard/popUpCard";
// import Timer from "../../components/timer/timer";
// import { useEffect } from "react";
// import animation from "../../utils/animation";

import Card from '../card/card';
import Icon from '../icon/icon';
import SectionIntro from '../sectionIntro/sectionIntro';
import SocialsIconLinks from '../shared/socialsIconLinks/socialsIconLinks';
import SubTitle from '../shared/subTitle/subTitle';
import Heading from '../shared/heading/heading';
import LinkBox from '../linkBox/linkBox';
import { useEffect } from 'react';
import animation from '../../utils/animation';
import Link from 'next/link';
import Image from 'next/image';

// const { default: Heading } = require('../shared/heading/heading');

// import { UseOurPartnerData } from "../../utils/teamsData";
const IndexPage = () => {
  useEffect(() => {
    animation.afterCallback(
      new IntersectionObserver(
        animation.instersectioOberserverCallback,
        animation.rootMargin
      )
    );
  }, []);

  const works = [
    {
      id: 0,
      url: 'https://www.behance.net/gallery/192254495/Lead-Generation-Form-Revamp',
      isLinkOut: false,
      newTab: false,
      cursorPointer: false,
      imgName: 'comingSoon',
      imgStartPosition: 'top',
      overlayText: 'Data Science AI Projects',
      overlaySubText: 'UX/UI Designer',
      title: 'Designing AI Products: Understanding Human-Machine Dynamics ',
      // genre: 'Understanding Human-Machine Dynamics',
      description: `In collaboration with Data Scientists and Clients, worked on products focused on AI. This project offers a glimpse into the process, from user interviews to high-fidelity designs - 
       <li> Challenges in Crafting User Experiences for AI </li>
    <li> WCAG-accessible: Colorblind-friendly palette generation—a framework. </li>`,
    },
    {
      id: 1,
      url: 'https://www.behance.net/gallery/192254495/Lead-Generation-Form-Revamp',
      isLinkOut: true,
      newTab: true,
      cursorPointer: true,
      imgName: 'card-edulead-new1',
      imgStartPosition: 'top',
      overlayText: 'EduLead GenForm Design',
      overlaySubText: 'Product/UX Designer',
      title: 'Streamlining Lead Generation: A Form Flow Redesign Journey',
      // genre: ' Peak Performance',
      description: `A walkthrough of revamping the form flow to boost lead generation, done in collaboration with business and product teams over four months.
<li> Resulted in an approximately 5% increase in lead submission rate within a month.</li>
<li> Sneak peek of post-release A/B test experiments for high performance.</li>`,
    },
    {
      id: 2,
      url: '/project/designSystem',
      isLinkOut: true,
      newTab: false,
      cursorPointer: true,
      imgName: 'card-design-system',
      imgStartPosition: 'top',
      overlayText: 'Design System',
      overlaySubText: 'UI/UX Designer and Research',
      title: 'Designing a Universal Framework for  Design Systems',
      // genre: 'for  Design Systems',
      description: `Conducted in-depth research to develop a comprehensive design blueprint for building a  design system library, addressing the challenges of complex product environments.
<li> Successfully implemented the structure across four diverse projects. </li>
<li> Scaled at the white-label level for broader application </li>
        `,
      // tags: ['UI/UX Design', 'System Design', 'Research'],
    },
    {
      id: 3,
      url: '/project/greenRWebsite',
      isLinkOut: true,
      newTab: false,
      cursorPointer: true,
      imgName: 'card-greenr',
      imgStartPosition: 'top',
      overlayText: 'GreenR Web Design',
      overlaySubText: 'UI Designer and Frontend Developer',
      title: 'Harmonizing Branding and Aesthetics for a Stunning Web Designs',
      // genre: ' for a Stunning Web Design',
      description: `Teamed up with a logo design expert and the client to blend my  expertise, delivering a visually engaging website that supports startups in achieving sustainability across environmental and commercial aspects.
<li>Achieved over 100 registrations within the first release.</li>`,
      // tags: [
      //   'UI Design',
      //   'Frontend Development',
      //   'Visual Design',
      //   'Client Project',
      // ],
    },
  ];

  // let introHeading = `A <span className="pinkText italic font-semibold"> Product Designer, </span> experienced in <span className="pinkText italic font-semibold"> Front-end Development </span> optimising digital interfaces through addressing <span className="pinkText italic font-semibold"> UX challenges </span> & crafting <span className="pinkText italic font-semibold"> Design systems.</span>`;
  let introHeading = `<span className="font-normal" > <b>Product/UX Designer </b> with a <b>frontend background</b>, dedicated to building <b>accessible</b> and <b>high-performance</b> experiences in <b>AI</b> and <b>SaaS</b> <span>`;
  let introDesc = `<span className="font-normal" > I design <i>(& sometimes code)</i> cool websites, including this one! :P </span>`;

  let introText = `<div>Hello</div> <div className="mt30">i m</div>  <div className="mt30">Quba</div>  `;

  return (
    <>
      <div className="relative hiddenScrollAnimation">
        <div className="potraitWrap "></div>
      </div>
      <div className="grid-main-container homeMain">
        <div className="grid-container">
          <div className="pt40 introWrap">
            <Link
              className="nameTag blackTag hiddenScrollAnimation"
              href="/aboutMe"
            >
              <SubTitle
                styleClass="hiddenScrollAnimation f16 para mt-[20px]"
                text={introText}
              />
              <Icon
                svgStyleClass={'w-[20px] mx-[20px] my-[10px] h-[30px] inline  '}
                name="arrowRight"
              />
            </Link>
            <div className="hiddenScrollAnimation hiddenHeadingAnimation introStyle">
              <Heading
                styleClass="f24 hiddenScrollAnimation "
                text={introHeading}
              />
              <SubTitle
                text={introDesc}
                styleType="fun"
                styleClass="caseStudyContent pt-[12px]  hiddenScrollAnimation hiddenHeadingAnimation"
              />
              <a
                href="/aboutMe"
                className="showInMobile primaryUnderlineLinks  caseStudyContent underline  hiddenScrollAnimation"
              >
                get to know me better
                <Icon
                  svgStyleClass={
                    'w-[30px] pl-1  h-[30px] inline mr-2 border-[#ff93ad] fill-[#ff93ad] hiddenScrollAnimation'
                  }
                  name="arrowRight"
                  fill="black"
                />{' '}
              </a>
              <SocialsIconLinks containerStyleClass="mt-[30px] pb-[20px] flex " />
            </div>
          </div>
        </div>
      </div>

      <div id="latestWork">
        <SectionIntro
          containerStyleClass="showInMobile mt60"
          text="Latest Work"
          bg="light"
        />
      </div>
      <div className="grid-main-container pt80" id="latestWork">
        <div className="grid-container">
          {works.map((work, i) => (
            <Card
              key={`key_card_${work.id}`}
              url={work.url}
              isLinkOut={work.isLinkOut}
              newTab={work.newTab}
              cursorPointer={work.cursorPointer}
              imgName={work.imgName}
              overlayText={work.overlayText}
              overlaySubText={work.overlaySubText}
              title={work.title}
              genre={work.genre}
              description={work.description}
              tags={work.tags}
              imgStartPosition={work.imgStartPosition}
              containerStyle={+i === works.length - 1 ? 'mb-8' : 'mb-12'}
            />
          ))}
          {/* <div className="flex justify-end">
            <LinkBox
              url="https://www.behance.net/quba_abdus/projects"
              iconName="goto"
              text="check out my other design projects on behance"
              className="w-fit"
              containerStyleClass="mt40 w-fit"
            />
          </div> */}
        </div>
      </div>

      <SectionIntro
        containerStyleClass="showInMobile mt60 "
        text="Contact Me"
        bg="light"
      />

      <div className="grid-main-container homeMain">
        <div className="grid-container">
          <div className="pt80 mb60 introWrap flex ">
            <Link
              className="blackTag  hiddenScrollAnimation"
              href="mailto:quba.abdus@gmail.com"
            >
              <SubTitle
                styleClass="hiddenScrollAnimation f16 para mt-[20px]"
                text="contact"
              />
              <Icon
                svgStyleClass={'w-[30px] mx-[20px] my-[10px] h-[30px] inline  '}
                name="arrowRight"
              />
            </Link>
            <div className="grid grid-rows-1 xs:grid-cols-1 sm:grid-cols-1 md:grid-cols-3 lg:grid-cols-3 gap-10">
              <div className="md:col-span-2 lg:col-span-2 caseStudyContent md:pr-32 lg:pr-32 tracking-wider leading-8">
                <p className="hiddenScrollAnimation pl-4">
                  I'm thrilled to explore the best of both worlds - full-time
                  and freelance. If you have any queries or simply want to
                  connect, feel free to drop me a message. Let's chat!
                </p>
                <div className=" pl-4 pt40 grid grid-rows-3 grid-cols-1 gap-1">
                  <h3 className="hiddenScrollAnimation font-bold">
                    Message me here
                  </h3>
                  <div className="flex items-center">
                    <Icon
                      svgStyleClass={
                        'w-[28px] h-[28px] inline mr-2  hiddenScrollAnimation'
                      }
                      name="linkedin"
                    />
                    <a
                      href="https://www.linkedin.com/in/quba-abdus/"
                      target="_blank"
                      className="primaryUnderlineLinks hiddenScrollAnimation"
                    >
                      Linkedin
                    </a>
                  </div>
                  <div className="flex items-center mt-1">
                    <Icon
                      svgStyleClass={
                        'w-[28px] h-[28px] inline mr-2  hiddenScrollAnimation'
                      }
                      name="mail"
                    />
                    <a
                      href="mailto:quba.abdus@gmail.com"
                      className="primaryUnderlineLinks hiddenScrollAnimation"
                    >
                      quba.abdus@gmail.com
                    </a>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default IndexPage;
