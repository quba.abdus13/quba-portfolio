import { useRouter } from 'next/router';
import Title from '../title/title';
import cardStyles from './card.module.scss';
import ImgOverlay from '../imgOverlay/imgOverlay';
import ReactMarkdown from 'react-markdown';
import rehypeRaw from 'rehype-raw';

const Card = props => {
  const {
    url,
    title,
    genre,
    description,
    tags,
    containerStyle,
    overlayText,
    overlaySubText,
    imgName,
    imgStartPosition,
    isLinkOut = true,
    cursorPointer = true,
    newTab = fasle,
  } = props;

  const router = useRouter();

  return (
    <div
      className={`grid grid-rows-1 xs:grid-cols-1 sm:grid-cols-1 md:grid-cols-3 lg:grid-cols-3 gap-10 ${containerStyle} ${cardStyles.card}`}
    >
      <div
        onClick={() => {
          if (!isLinkOut) {
            return;
          }

          if (newTab) {
            window.open(url);
            console.log('yes new', newTab);
          } else {
            router.push({ pathname: url });
            console.log('no new', newTab);
          }
        }}
        className="h-[250px] test md:h-[350px] lg:h-[350px]"
      >
        <ImgOverlay
          imgName={imgName}
          imgStartPosition={imgStartPosition}
          overlayText={overlayText}
          subText={overlaySubText}
          isLinkOut={isLinkOut}
          cursorPointer={cursorPointer}
          hideHoverOverlayOnHover={false}
          isSubtext={true}
        />
      </div>
      <div className="md:col-span-2 lg:col-span-2">
        <Title styleClass="hiddenScrollAnimation inline" title={`${title} `} />
        <Title styleClass={`hiddenScrollAnimation inline `} title={genre} />
        {/* <p className="hiddenScrollAnimation mt-[20px] caseStudyContent f18">
          {description}
        </p> */}
        <ReactMarkdown
          className={`hiddenScrollAnimation caseStudyContent mt-[20px] caseStudyContent f18`}
          children={description}
          rehypePlugins={[rehypeRaw]}
        ></ReactMarkdown>
        {tags &&
          tags.map((tag, i) => (
            <li
              key={`key_card_${tag}_${i}`}
              className={`inline-block textGrey mr-5 px-5 py-2 mt-5 ${cardStyles.tag}`}
            >
              <p className="hiddenScrollAnimation">{tag}</p>
            </li>
          ))}
      </div>
    </div>
  );
};

export default Card;
