import ReactMarkdown from 'react-markdown';
import rehypeRaw from 'rehype-raw';

const Disclaimer = props => {
  const { disclaimerContent } = props;
  return (
    <div className="grid-main-container mt40 pb60">
      <div className="grid-container">
        <ReactMarkdown
          className={` mt40 italic  lowercase text-right f16 opacity-50 hiddenScrollAnimation`}
          children={disclaimerContent}
          rehypePlugins={[rehypeRaw]}
        ></ReactMarkdown>
        {/* </div> */}
      </div>
    </div>
  );
};

export default Disclaimer;
