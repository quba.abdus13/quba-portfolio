import footerStyle from './footer.module.scss';
import SocialsIconLinks from '../shared/socialsIconLinks/socialsIconLinks';
import Icon from '../icon/icon';

const Footer = () => {
  return (
    <>
      <footer className={`${footerStyle.footer} f16 `}>
        <div className="grid-main-container text-center md:text-start lg:text-start">
          {/* <div className="grid-container grid xs:grid-cols-1 sm:grid-cols-1 md:grid-cols-3 lg:grid-cols-3 gap-10">
            <div className="md:col-span-2 lg:col-span-2 flex flex-col md:flex-row lg:flex-row">
              <div className="mb-5 md:mr-5 lg:mr-5">
                <a
                  href="/"
                  className="flex items-center justify-center hiddenScrollAnimation"
                >
                  <img
                    src="/images/q-logo.png"
                    className="h-[60px] md:h-[80px] lg:h-[80px]"
                    alt="Q logo"
                  />
                </a>
              </div>
              <div>
                <p className="mb-5 hiddenScrollAnimation">
                  Thanks for stopping by!
                </p>
                <p className="hiddenScrollAnimation">Let's chat?</p>
                <a
                  href="mailto:quba.abdus@gmail.com"
                  className="primaryUnderlineLinks hiddenScrollAnimation"
                >
                  quba.abdus@gmail.com
                </a>
              </div>
            </div>
            <SocialsIconLinks containerStyleClass="col-span-1 flex justify-center md:justify-end lg:justify-end hiddenScrollAnimation" />
          </div> */}
          <div
            className={`grid-container py-10 text-center mb-1 ${footerStyle.copyright}`}
          >
            <p className="f14 flex justify-center items-center hiddenScrollAnimation">
              <Icon
                name="copyright"
                color="currentColor"
                svgStyleClass="inline h-[14px] w-[14px] mr-1"
              />
              Copyright 2025. Made by Quba Abdus
            </p>
          </div>
        </div>
      </footer>
    </>
  );
};

export default Footer;
