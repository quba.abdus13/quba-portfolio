import React, { useEffect } from 'react';
import { initAccordions } from 'flowbite';
import { useRouter } from 'next/router';
import headerStyles from './header.module.scss';
import Link from 'next/link';

const Header = () => {
  useEffect(() => {
    initAccordions();
  }, []);

  const router = useRouter();

  const navLinks = [
    { url: '/', text: 'Home', target: '_self' },
    // { url: '/#latestWork', text: 'Works', target: '_self' },
    { url: '/aboutMe', text: 'About Me', target: '_self' },
    { url: '/quba_designer_v2.pdf', text: 'Resume', target: '_blank' },
  ];

  return (
    <nav className="mainNav">
      <div className="max-w-screen-xl p-4 md:p-0 flex flex-wrap items-center justify-end mx-auto ">
        {/* <a href="/" className="flex items-center">
          <img src="/images/q-logo.png" className="h-[50px]" alt="Q logo" />
        </a> */}
        <button
          data-collapse-toggle="navbar-default"
          type="button"
          className="md:hidden"
          aria-controls="navbar-default"
          aria-expanded="false"
        >
          <span className="sr-only">Open main menu</span>
          <svg
            className="w-6 h-6"
            aria-hidden="true"
            fill="currentColor"
            viewBox="0 0 20 20"
            xmlns="http://www.w3.org/2000/svg"
          >
            <path
              fillRule="evenodd"
              d="M3 5a1 1 0 011-1h12a1 1 0 110 2H4a1 1 0 01-1-1zM3 10a1 1 0 011-1h12a1 1 0 110 2H4a1 1 0 01-1-1zM3 15a1 1 0 011-1h12a1 1 0 110 2H4a1 1 0 01-1-1z"
              clipRule="evenodd"
            ></path>
          </svg>
        </button>
        <div className="hidden w-full md:block md:w-auto" id="navbar-default">
          <ul className="font-medium flex flex-col  md:p-0 mt-4 md:flex-row md:space-x-8 md:mt-0 uppercase ${headerStyles.regularLink}">
            {navLinks.map((v, i) => (
              <li className="mr-0 mX0 ml-0" key={`key_navLinks_${i}`}>
                <Link
                  href={v.url}
                  target={v.target}
                  className={`block py-4 px-6  mX0 ml-0 mr-0  hiddenScrollAnimation colorHoverAnimation ${
                    headerStyles.link
                  } ${
                    router.pathname === v.url
                      ? headerStyles.activeLink
                      : headerStyles.regularLink
                  }`}
                >
                  {v.text}
                </Link>
              </li>
            ))}
            {/* <li>
              <a href="/" className="block py-2 pl-3 pr-4" aria-current="page">
                Home
              </a>
            </li>
            <li>
              <a href="/#latestWork" className="block py-2 pl-3 pr-4">
                Works
              </a>
            </li>
            <li>
              <a href="/aboutMe" className="block py-2 pl-3 pr-4">
                About Me
              </a>
            </li> */}
          </ul>
        </div>
      </div>
    </nav>
  );
};

export default Header;
