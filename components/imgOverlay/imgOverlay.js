import Image from 'next/image';
import Icon from '../icon/icon';
import Link from 'next/link';

const ImgOverlay = props => {
  let {
    overlayText,
    subText,
    noOverLay = false,
    imgName = 'bath',
    imgFormat = 'png',
    hideHoverOverlayOnHover = true,
    isLinkOut = false,
    cursorPointer = true,
    linkBlockStyleClass = 'linkBlock',
    textStyleClass,
    isSubtext = false,
    imgStartPosition = '',
  } = props;

  return (
    <div
      className={`h-full w-full relative imgMainOverlay ${
        isLinkOut ? 'imgMainOverlayAnimation' : ''
      } ${cursorPointer ? 'cursor-pointer' : ''}`}
    >
      {cursorPointer ? (
        <div className={linkBlockStyleClass}>
          <Icon
            svgStyleClass="h-full w-full p-[8px]"
            color="white"
            name="arrowRight"
          />
        </div>
      ) : (
        ``
      )}

      <div
        className={`${noOverLay ? '' : 'overlay bgHoverAnimation'} absolute ${
          hideHoverOverlayOnHover ? 'whiteOverlay' : 'primaryOverlay'
        }  `}
      ></div>
      <div className="w-full h-full relative">
        <div
          className={`${
            textStyleClass
              ? textStyleClass
              : hideHoverOverlayOnHover
              ? 'text-white hoverPink textShadow'
              : 'text-white textShadow'
          } px-1 absolute overlayText font-semibold`}
        >
          <div className={` defaultCardtext px-1 font-semibold`}>
            {overlayText}
          </div>
          {isSubtext ? (
            <div className={` px-1 defaultCardSubtext`}>{subText}</div>
          ) : (
            ''
          )}
        </div>

        <div className="h-full w-full relative">
          <Image
            src={`/images/${imgName}.${imgFormat}`}
            alt="Overlay image"
            fill
            style={{ objectFit: 'cover', objectPosition: imgStartPosition }}
          />
        </div>
      </div>
    </div>
  );
};

export default ImgOverlay;
