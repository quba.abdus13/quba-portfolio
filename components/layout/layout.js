import Footer from '../footer/footer';
import Header from '../header/header';
import ScrollToTop from 'react-scroll-to-top';
import LayoutStyles from '../layout/layout.module.scss';

const Layout = ({ children }) => {
  return (
    <div className="bg-p-400 mt-[51px]">
      <Header />
      {children}
      <ScrollToTop
        smooth
        className={`${LayoutStyles.scrollToTop} bgHoverAnimation`}
        color="#00000"
      />
      <Footer />
    </div>
  );
};

export default Layout;
