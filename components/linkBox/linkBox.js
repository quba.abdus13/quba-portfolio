import Icon from '../icon/icon';
import linkBoxStyle from './linkBox.module.scss';

const LinkBox = props => {
  const {
    url,
    text,
    iconName = 'link',
    containerStyleClass,
    iconStyleClass,
  } = props;

  return (
    <div
      className={`flex items-center ${containerStyleClass} ${linkBoxStyle.all} bgHoverAnimation  `}
    >
      <Icon
        name={iconName}
        svgStyleClass={`${iconStyleClass} h-[20px] w-[20px] mr-2`}
      />
      <a href={url} target="_blank" className="block letterSpace">
        {text}
      </a>
    </div>
  );
};

export default LinkBox;
