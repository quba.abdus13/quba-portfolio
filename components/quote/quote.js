import Icon from '../icon/icon';
import quoteStyle from './quote.module.scss';

const Quote = props => {
  const { text, author, containerStyle } = props;

  return (
    <div className={`flex ${quoteStyle.quoteBox} ${containerStyle}`}>
      <Icon
        name="quote"
        externalDivClass="w-[30px] md:w-[60px] lg:w-[60px] h-[30px] mr-4"
      />
      <p className="italic expDuration hiddenScrollAnimation">
        {text} ~ {author}
      </p>
    </div>
  );
};

export default Quote;
