import sectionIntroStyle from './sectionIntro.module.scss';

const SectionIntro = props => {
  const {
    containerStyleClass,
    styleClass,
    text,
    isLeftLine = false,
    bg = 'dark',
  } = props;
  const styleClassName = `uppercase text-sm tracking-widest ${styleClass}`;
  const bgClass = bg === 'dark' ? 'darkBg' : 'lightBg';

  return (
    <div
      className={`flex items-center ${
        bg == 'darkBg' ? 'text-white' : ''
      }  ${containerStyleClass}`}
    >
      {/* {isLeftLine && <div className={`${sectionIntroStyle.line}`}></div>} */}
      <div className={`grid-main-container z-10 ${bgClass}`}>
        <p
          className={`grid-container ml-1 font-semibold hiddenScrollAnimation ${styleClassName}`}
        >
          {text}
        </p>
      </div>
    </div>
  );
};

export default SectionIntro;
