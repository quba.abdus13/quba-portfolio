import headingStyle from './heading.module.scss';
import ReactMarkdown from 'react-markdown';
import rehypeRaw from 'rehype-raw';

const Heading = props => {
  const { styleClass, text } = props;
  const styleClassName = ` font-semibold f32 hiddenScrollAnimation ${styleClass} hiddenHeadingAnimation`;

  return (
    <ReactMarkdown
      className={styleClassName}
      children={text}
      rehypePlugins={[rehypeRaw]}
    ></ReactMarkdown>
    // <h1 className={styleClassName}>{text}</h1>;
  );
};

export default Heading;
