import Icon from '../../icon/icon';
import socialsIconLinksStyle from './socialsIconLinks.module.scss';

const SocialsIconLinks = props => {
  const iconLinkStyle = `w-[28px] h-[28px] inline fillHoverAnimation hover:cursor-pointer ${socialsIconLinksStyle.icon}`;

  return (
    <div className={`${props.containerStyleClass} flex`}>
      <div
        className="pr-1"
        onClick={() => window.open('https://dribbble.com/Quba_13', '_blank')}
      >
        <Icon svgStyleClass={`${iconLinkStyle} mr-3`} name="dribble" />
      </div>
      <div
        className="pr-1"
        onClick={() =>
          window.open('https://www.linkedin.com/in/quba-abdus/', '_blank')
        }
      >
        <Icon svgStyleClass={`${iconLinkStyle} mr-3`} name="linkedin" />
      </div>
      <div
        className="pr-1 pt-1"
        onClick={() => (window.location.href = 'mailto:quba.abdus@gmail.com')}
      >
        <Icon svgStyleClass={`${iconLinkStyle} mr-3`} name="mail" />
      </div>
      <div
        className="pr-1 pt-1"
        onClick={() =>
          window.open('https://www.behance.net/quba_abdus/projects', '_blank')
        }
      >
        <Icon svgStyleClass={`${iconLinkStyle}`} name="behance" />
      </div>
    </div>
  );
};

export default SocialsIconLinks;
