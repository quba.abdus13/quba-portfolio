import subTitleStyle from './subTitle.module.scss';
import ReactMarkdown from 'react-markdown';
import rehypeRaw from 'rehype-raw';

const SubTitle = props => {
  const { styleClass, text, styleType } = props;

  let styleClassName = ` ${styleClass} `;

  if (styleType === 'fun') {
    styleClassName += 'lowercase';
  }
  if (styleType === 'normal') {
    styleClassName += 'capitalize';
  }
  if (styleType === 'formal') {
    styleClassName += 'text-white uppercase';
  } else {
  }

  return (
    <ReactMarkdown
      className={styleClassName}
      children={text}
      rehypePlugins={[rehypeRaw]}
    ></ReactMarkdown>
  );
};

export default SubTitle;
