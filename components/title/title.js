import titleStyle from './title.module.scss';

const Title = props => {
  const { styleObj, styleClass, title } = props;

  return (
    <p style={styleObj} className={`f28 ${titleStyle.mainTitle} ${styleClass}`}>
      {title}
    </p>
  );
};

export default Title;
