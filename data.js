const works = [
  {
    id: 0,
    url: '/project/collegeEnroll',
    title: 'College Enroll Case Study',
    genre: 'Case Study',
    description:
      'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore',
    tags: ['UI/UX', 'Ed Tech', 'Educationla'],
    research1:
      'When I joined the team, we had a really mature product, but since there was no systematic approach or process for the designing and implementing the design there were lots of design inconsistencies.',
    research2:
      'When I got my first design task which was basically a new feature implementation which required me to come up with a new component. I remember searching for the design system and being extremely confused by being lost.',
    quotes: [
      {
        text: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore  coding, illustrations art projects',
        author: 'senior developer',
      },
      {
        text: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore  coding, illustrations art projects',
        author: 'senior developer',
      },
    ],
    textBelowQuotes:
      'This basically resulted in us proposing the design system side project which eventually turned out to be a fulltime project resulting in massive success by being implemented by 3 other publishers and also in a workshop with the other intra design teams.',
    image: '',
    textBelowImages:
      'A design system is a unified language that helps a team solve problems consistently. In simple words, a design system consists of all the colors, styles, typography, grids, components, icons, assets, and much more. These things are all created by designers and kept in a library. The developer follows these guidelines while developing the designs, this helps in consistency of the product overall.',
    whatWeHadToWorkWith: [
      'Small team including 2 designers',
      'More like a side project, could dedicate our 100% time to it at the beginning.',
      'Main focus was to improve the designs, reduce the inconsistency among the component, build a common blueprint/ manual to look up when lost for our current and future team members',
    ],
  },
  {
    id: 1,
    url: '/project/greenRWebsite',
    title: 'GreenR Website Case Study',
    genre: 'Case Study',
    description:
      'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore',
    tags: ['UI/UX', 'Environment'],
    research1:
      'When I joined the team, we had a really mature product, but since there was no systematic approach or process for the designing and implementing the design there were lots of design inconsistencies.',
    research2:
      'When I got my first design task which was basically a new feature implementation which required me to come up with a new component. I remember searching for the design system and being extremely confused by being lost.',
    quotes: [
      {
        text: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore  coding, illustrations art projects',
        author: 'senior developer',
      },
      {
        text: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore  coding, illustrations art projects',
        author: 'senior developer',
      },
    ],
    textBelowQuotes:
      'This basically resulted in us proposing the design system side project which eventually turned out to be a fulltime project resulting in massive success by being implemented by 3 other publishers and also in a workshop with the other intra design teams.',
    image: '',
    textBelowImages:
      'A design system is a unified language that helps a team solve problems consistently. In simple words, a design system consists of all the colors, styles, typography, grids, components, icons, assets, and much more. These things are all created by designers and kept in a library. The developer follows these guidelines while developing the designs, this helps in consistency of the product overall.',
    whatWeHadToWorkWith: [
      'Small team including 2 designers',
      'More like a side project, could dedicate our 100% time to it at the beginning.',
      'Main focus was to improve the designs, reduce the inconsistency among the component, build a common blueprint/ manual to look up when lost for our current and future team members',
    ],
  },
  {
    id: 2,
    url: '/project/designSystem',
    title: 'Design System Case Study',
    genre: 'Case Study',
    description:
      'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore',
    tags: ['UI/UX', 'System Design'],
    research1:
      'When I joined the team, we had a really mature product, but since there was no systematic approach or process for the designing and implementing the design there were lots of design inconsistencies.',
    research2:
      'When I got my first design task which was basically a new feature implementation which required me to come up with a new component. I remember searching for the design system and being extremely confused by being lost.',
    quotes: [
      {
        text: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore  coding, illustrations art projects',
        author: 'senior developer',
      },
      {
        text: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore  coding, illustrations art projects',
        author: 'senior developer',
      },
    ],
    textBelowQuotes:
      'This basically resulted in us proposing the design system side project which eventually turned out to be a fulltime project resulting in massive success by being implemented by 3 other publishers and also in a workshop with the other intra design teams.',
    image: '',
    textBelowImages:
      'A design system is a unified language that helps a team solve problems consistently. In simple words, a design system consists of all the colors, styles, typography, grids, components, icons, assets, and much more. These things are all created by designers and kept in a library. The developer follows these guidelines while developing the designs, this helps in consistency of the product overall.',
    whatWeHadToWorkWith: [
      'Small team including 2 designers',
      'More like a side project, could dedicate our 100% time to it at the beginning.',
      'Main focus was to improve the designs, reduce the inconsistency among the component, build a common blueprint/ manual to look up when lost for our current and future team members',
    ],
  },
];

const data = { works };

export default data;
