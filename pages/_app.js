import Head from 'next/head';
// import Header from "../components/header/header";
import Layout from '../components/layout/layout';
import Script from 'next/script';
import '../styles/globals.scss';
import { useEffect } from 'react';
import { useRouter } from 'next/router';
import * as gtag from '../utils/gtag';

export default function App({ Component, pageProps }) {
  const router = useRouter();

  useEffect(() => {
    const handleRouteChange = url => {
      gtag.pageview(url); // Call the helper function
    };

    router.events.on('routeChangeComplete', handleRouteChange);
    return () => {
      router.events.off('routeChangeComplete', handleRouteChange);
    };
  }, [router.events]);

  return (
    <>
      <Script src="https://www.googletagmanager.com/gtag/js?id=G-XV9S69P8ET" />

      <Script>
        {`
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());
        gtag('config', 'G-XV9S69P8ET');
        `}
      </Script>
      {/* <link
        href="https://fonts.googleapis.com/css2?family=Source+Sans+Pro:ital,wght@0,200;0,300;0,400;0,600;0,700;0,900;1,200;1,300;1,400;1,600;1,700;1,900&display=swap"
        rel="stylesheet"
      ></link> */}
      <link
        href="https://fonts.googleapis.com/css2?family=Mulish:ital,wght@0,200..1000;1,200..1000&display=swap"
        rel="stylesheet"
      ></link>
      <Head>
        <link rel="shortcut icon" href="/favicon.png" />
        <title>Quba Abdus | Designer Portfolio</title>
      </Head>
      {/* <Header /> */}
      <Layout>
        <Component {...pageProps} />
      </Layout>
      {/* <Script src="https://cdn.jsdelivr.net/npm/tw-elements/dist/js/index.min.js" /> */}
    </>
  );
}
