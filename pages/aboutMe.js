import SectionIntro from '../components/sectionIntro/sectionIntro';
import Heading from '../components/shared/heading/heading';
import SubTitle from '../components/shared/subTitle/subTitle';
import ImgOverlay from '../components/imgOverlay/imgOverlay';
import Icon from '../components/icon/icon';
import { useEffect } from 'react';
import animation from '../utils/animation';
import Link from 'next/link';

const AboutMe = () => {
  useEffect(() => {
    animation.afterCallback(
      new IntersectionObserver(
        animation.instersectioOberserverCallback,
        animation.rootMargin
      )
    );
  }, []);

  const otherProjects = [
    { text: 'Digital Illustrations', imageName: 'digital-illustration' },
    { text: 'Frontend Development', imageName: 'frontend-development' },
    {
      text: 'Traditional Illustrations',
      imageName: 'traditional-illustration',
    },
    { text: 'Traditional Art', imageName: 'traditional-art' },
    { text: 'Digital Art', imageName: 'digital-art' },
  ];

  const outsideWork = [
    { text: 'Places and Travelling', imageName: 'exploring-food' },
    { text: 'Food and Coffee', imageName: 'cafe-coffee' },
    { text: 'Exploring and Art', imageName: 'uae2' },
  ];

  const experience = [
    { role: 'UX/UI Designer @Data Society', duration: 'June 2024 - Present' },
    {
      role: 'UX/Product Designer @Media.net',
      duration: 'April 2022 - March 2024',
    },
    {
      role: 'Product Designer / SE @Accionlabs',
      duration: 'October 2019 - April 2022 ',
    },
    { role: 'Freelance', duration: 'October 2019 - June 20244' },
  ];

  const openInNewTab = url => {
    const newWindow = window.open(url, '_blank', 'noopener,noreferrer');
    if (newWindow) newWindow.opener = null;
  };

  let mainIntro = `<span className="font-normal" >With a focus on <i> accessibility & performance </i>, I’ve built everything from <i>detailed dashboards to crisp web pages, smooth form flows, and adaptable design systems.</i></span>`;

  return (
    <>
      <div className="darkBgContainer pb60 lightBg text-left">
        <div className="grid-main-container  text-left">
          <div className="grid-container pt60 hiddenScrollAnimation hiddenHeadingAnimation">
            <SubTitle
              text="An Experienced Designer"
              styleType="fun"
              styleClass="mt-[20px] caseStudyContent hiddenScrollAnimation hiddenHeadingAnimation"
            />
            <Heading
              styleClass="w-[100%] md:w-[70%] pt-1 pb-6 hiddenScrollAnimation hiddenHeadingAnimation"
              text={mainIntro}
            />
            <SubTitle
              text="who am i if not a designer? i'm glad you asked!"
              styleType="fun"
              styleClass="caseStudyContent  hiddenScrollAnimation hiddenHeadingAnimation"
            />
            <a
              href="/#latestWork"
              className="primaryUnderlineLinks whiteLink caseStudyContent underline  hiddenScrollAnimation"
            >
              explore my work
              <Icon
                svgStyleClass={
                  'w-[30px] pl-1  h-[30px] inline mr-2 border-[#ff93ad] fill-[#ff93ad] hiddenScrollAnimation'
                }
                name="arrowRight"
              />{' '}
            </a>
          </div>
        </div>
      </div>

      <div className="grid-main-container pb40 pt80 homeMain">
        <div className="grid-container">
          <div className="grid md:grid-cols-3 lg:grid-cols-3 gap-10">
            {outsideWork.map((v, i) => (
              <div
                key={`key_otherProjects_${i}`}
                className="h-[250px] test md:h-[350px] lg:h-[350px]"
              >
                <ImgOverlay
                  overlayText={v.text}
                  imgName={v.imageName}
                  isLinkOut={true}
                  cursorPointer={false}
                />
              </div>
            ))}
          </div>
        </div>
      </div>

      <div>
        <SectionIntro
          containerStyleClass="showInMobile mt60"
          text="Experience"
          bg="light"
        />
      </div>
      <div className="grid-main-container homeMain pt80">
        <div className="grid-container grid md:grid-cols-2 lg:grid-cols-2 gap-10 lg:gap-20">
          <div className="flex">
            <Link
              className="blackTag  hiddenScrollAnimation"
              href="/#latestWork"
            >
              <SubTitle
                styleClass="hiddenScrollAnimation f16 para mt-[20px]"
                text="experience"
              />
              <Icon
                svgStyleClass={'w-[20px] mx-[20px] my-[10px] h-[30px] inline  '}
                name="arrowRight"
              />
            </Link>

            <div className="pl40 w-[100%]">
              {experience.map((v, i) => (
                <div key={`key_experience_${i}`} className="mb-[20px]">
                  <p className="expCompany hiddenScrollAnimation">{v.role}</p>
                  <p className="expDuration hiddenScrollAnimation">
                    {v.duration}
                  </p>
                </div>
              ))}
            </div>
          </div>

          <div>
            <p className="caseStudyContent hiddenScrollAnimation pb-5">
              As a UX Designer at Data Society, I design AI and SAAS products
              with a strong focus on <b>accessibility</b> and{' '}
              <b>customer retention.</b>
            </p>
            <p className="caseStudyContent hiddenScrollAnimation pb-5">
              My role involves creating complex{' '}
              <b>design systems, conducting user research,</b> and building
              intuitive <b>web and mobile </b> interfaces—from{' '}
              <b>user interviews to the final client handover.</b>
            </p>
            <p className="caseStudyContent hiddenScrollAnimation pb-5">
              Previously, I worked on <b> lead-gen and white-label products,</b>
              driving <b>A/B testing</b> efforts to improve key{' '}
              <b>metrics like submission and return rates.</b>
            </p>
            <p className="caseStudyContent hiddenScrollAnimation pb-5">
              I embarked on my journey as a software engineer and discovered my
              passion for Product design. With a natural inclination towards
              art, I’m excited to combine my passion for creativity and
              technology in my full-time role.
            </p>
            <p className="caseStudyContent hiddenScrollAnimation">
              The impact? Delighted users and thriving businesses.
            </p>
          </div>
        </div>
      </div>

      <div>
        <SectionIntro
          containerStyleClass="showInMobile mt60"
          text="Outside Work"
          bg="light"
        />
      </div>
      <div className="grid-main-container pt80 pb60">
        <div className="grid-container">
          <div className="grid md:grid-cols-3 lg:grid-cols-3 gap-10">
            {otherProjects.map((v, i) => (
              <div
                key={`key_otherProjects_${i}`}
                className="h-[250px] md:h-[350px] lg:h-[350px]"
              >
                <ImgOverlay
                  overlayText={v.text}
                  isLinkOut={true}
                  cursorPointer={false}
                  imgName={v.imageName}
                />
              </div>
            ))}
            <div
              className="p-3 h-[250px] md:h-[350px] lg:h-[350px] darkBgContainer hiddenScrollAnimation flex items-end hover:cursor-pointer  hover:bg-[#e58fa4]"
              onClick={() =>
                openInNewTab('https://www.behance.net/quba_abdus/projects')
              }
            >
              <p className="w-[80%] cardheading font-semibold">
                Check out my other design projects on Behance
              </p>
              <Icon
                name="arrowRight"
                svgStyleClass="w-[14%] h-[80px]"
                color="white"
              />
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default AboutMe;
