import Heading from '../../components/shared/heading/heading';

const CollegeEnroll = () => {
  return (
    <>
      <div className="grid-main-container text-center projectPageIntro">
        <div className="grid-container">
          <Heading text={`College <span className="pinkText">Enroll</span>`} />
          <p className="textPrimary py-5">Case Study</p>
          <p className="uppercase tracking-widest">UI / UX Designer</p>
        </div>
      </div>
    </>
  );
};

export default CollegeEnroll;
