import Disclaimer from '../../components/disclaimer/disclaimer';
import LinkBox from '../../components/linkBox/linkBox';
import Quote from '../../components/quote/quote';
import SectionIntro from '../../components/sectionIntro/sectionIntro';
import Heading from '../../components/shared/heading/heading';
import Image from 'next/image';
import { useEffect } from 'react';
import animation from '../../utils/animation';
import SubTitle from '../../components/shared/subTitle/subTitle';

const DesignSystem = () => {
  useEffect(() => {
    animation.afterCallback(
      new IntersectionObserver(
        animation.instersectioOberserverCallback,
        animation.rootMargin
      )
    );
  }, []);

  const stats = [
    { tag: 'Role', text: 'UI/UX Designer and Research' },
    { tag: 'Tools', text: 'Figma, Freeform and Adobe Illustrator' },
  ];

  const references = [
    {
      title: 'Atomic Design Methodology by Brad Frost',
      link: 'https://atomicdesign.bradfrost.com/chapter-2/#:~:text=Atomic%20design%20is%20atoms%2C%20molecules,parts%20at%20the%20same%20time',
    },
    {
      title: 'Figma: Desing System Software',
      link: 'https://www.figma.com/design-systems/',
    },
  ];

  const designSystemDisclaimer = `<p className="textGrey">
  ~ All information expressed and design systems showcased in this case
  study are my own and does not reflect the views or belong to any
  organization.
</p>
<p className="textGrey">
  Please do not share or upload the content or images from this case
  study without consent.
</p>
<p className="textGrey">
  I do not own the icons used in this case study, credit to the
  owners.
</p>`;

  return (
    <>
      <div className="darkBgContainer">
        <div className="grid-main-container text-center projectPageIntro">
          <div className="grid-container text-center">
            <Heading text={`Design <span className="pinkText">System</span>`} />
            <SubTitle
              text="A structured approach for the development of design system libraries customized for complex products, effectively addressing challenges in intricate product environments."
              styleType="normal"
              styleClass="pt-3 caseStudyContent hiddenScrollAnimation hiddenHeadingAnimation w-[100%] md:w-[70%] mx-auto"
            />
            <p className="textPrimary py-5 hiddenScrollAnimation">
              A Case Study
            </p>
            <p className="uppercase tracking-widest hiddenScrollAnimation">
              UI / UX Designer
            </p>
          </div>
        </div>

        <div className="lightBg text-black pt60 pb60">
          <div className="grid-main-container">
            <div className="grid-container grid grid-cols-1 md:grid-cols-2 lg:grid-cols-2 gap-5 md:gap-10 lg:gap-10">
              <div>
                {stats.map((v, i) => (
                  <div
                    key={`key_stats_${i}`}
                    className={`${i === stats.length - 1 ? '' : 'mb10'}`}
                  >
                    <span className="font-bold mr-2 hiddenScrollAnimation">
                      {v.tag}
                    </span>
                    <p className="inline hiddenScrollAnimation">{v.text}</p>
                  </div>
                ))}
              </div>
              <div>
                <span className="font-bold mb10 block hiddenScrollAnimation">
                  References
                </span>
                {references.map((v, i) => (
                  <div
                    key={`key_references_${i}`}
                    className={`pinkOnHover ${
                      i === references.length - 1 ? '' : 'mb10'
                    }`}
                  >
                    <a
                      href={v.link}
                      className="underline hiddenScrollAnimation"
                      target="_blank"
                    >
                      {v.title}
                    </a>
                  </div>
                ))}
              </div>
            </div>
          </div>
        </div>
        <SectionIntro
          containerStyleClass="mt60"
          text="Problem: What lead to the creation of the design system?"
        />
        <div className="grid-main-container mt40 pb60">
          <div className="grid-container">
            {/* <p className="uppercase text-sm font-semibold tracking-widest textPrimary hiddenScrollAnimation">
            Problem: What lead to the creation of the design system?
          </p> */}
            <div className="pt-2.5 grid grid-cols-1 md:grid-cols-2 lg:grid-cols-2 gap-5 md:gap-10 lg:gap-10">
              <ul className="list-disc caseStudyContent">
                <li className="hiddenScrollAnimation">
                  We had a product with a maturity of 2.5 years.
                </li>
                <li className="hiddenScrollAnimation">
                  The team included only one designer excluding me with strict
                  deadlines and crazy design requirements so the design process
                  had kind of fallen behind.
                </li>
              </ul>
              <ul className="list-disc caseStudyContent">
                <li className="hiddenScrollAnimation">
                  The team had also recently transitioned from Sketch to Figma,
                  so the file organization and handover process also needed some
                  improvement.
                </li>
              </ul>
            </div>
            <div className="mt40 grid grid-cols-1 md:grid-cols-4 lg:grid-cols-4 gap-5 md:gap-10 lg:gap-10">
              <div className="col-span-2">
                <div className="flex pt-3.5 justify-center items-center ">
                  <Image
                    src="/images/overview-1.png"
                    width={400}
                    height={200}
                    alt="lo fi wireframes image"
                  />
                </div>
              </div>
              <div className="col-span-2 ">
                <Quote
                  text="Yes, we should have.. But we don't"
                  author="Internal Designer"
                  containerStyle="mb-5"
                />
                <Quote
                  text="Great! would love it if we can create one"
                  author="Senior Developer"
                />
              </div>
            </div>
            <ul className="mt40 list-disc caseStudyContent">
              <li className="hiddenScrollAnimation">
                We designers shared a common persona with the developers and we
                realized, that we needed a design system in place for our team
                and product.
              </li>
            </ul>
          </div>
        </div>
        <SectionIntro containerStyleClass="mt60" text="Requirements" />
        <div className="mt40 grid-main-container pb60">
          <div className="grid-container">
            {/* <p className="uppercase text-sm font-semibold tracking-widest textPrimary hiddenScrollAnimation">
            Our goals
          </p> */}
            <ul className="list-disc caseStudyContent">
              <li className="hiddenScrollAnimation">
                <p className="mt40 uppercase text-sm font-semibold tracking-widest textPrimary hiddenScrollAnimation">
                  Clear-minded team members
                </p>
                Reduce the confusion the frustrations among the team due to lack
                of defined rules protocols
              </li>
              <li className="hiddenScrollAnimation">
                <p className="mt40 uppercase text-sm font-semibold tracking-widest textPrimary hiddenScrollAnimation">
                  <li className="hiddenScrollAnimation">
                    <p className="mt40 uppercase text-sm font-semibold tracking-widest textPrimary hiddenScrollAnimation">
                      Achieve design consistency across the product.
                    </p>
                  </li>
                </p>
                Eliminate the design inconsistency of the product overall.
                Duplicate components with same use cases
              </li>
              <li className="hiddenScrollAnimation">
                <p className="mt40 uppercase text-sm font-semibold tracking-widest textPrimary hiddenScrollAnimation">
                  Guidelines/Blueprint
                </p>
                Clear set of protocols for implementing or designing new
                components
              </li>
              <li className="hiddenScrollAnimation">
                <p className="mt40 uppercase text-sm font-semibold tracking-widest textPrimary hiddenScrollAnimation">
                  Design System Library
                </p>
                Build a system in Figma and publish it as a library. Treat it as
                a whitelable product.
              </li>
            </ul>
            <div className="mt40 flex justify-center">
              <Image
                src="/images/designInconsistency.png"
                width={1000}
                height={100}
                alt="lo fi wireframes image"
              />
            </div>
          </div>
        </div>
        <SectionIntro
          containerStyleClass="mt60"
          text="Initial research findings"
        />
        <div className="mt40 grid-main-container pb60">
          <div className="grid-container">
            <ul className="list-disc caseStudyContent">
              <li className="hiddenScrollAnimation">
                <p className="mt40 uppercase text-sm font-semibold tracking-widest textPrimary hiddenScrollAnimation">
                  Streamlined Guidelines for Time-Pressed Devs.
                </p>
                Due to time constraints faced by developers, concise guidelines
                are preferred, omitting excessive detail.
              </li>
              <li className="hiddenScrollAnimation">
                <p className="mt40 uppercase text-sm font-semibold tracking-widest textPrimary hiddenScrollAnimation">
                  <li className="hiddenScrollAnimation">
                    <p className="mt40 uppercase text-sm font-semibold tracking-widest textPrimary hiddenScrollAnimation">
                      The Less the more!
                    </p>
                  </li>
                </p>
                The principle of minimal color palette, shadows, and variants
                has consistently demonstrated its effectiveness and widespread
                usage.
              </li>
            </ul>
            <div className="mt40 flex justify-center">
              <Quote
                text="I prefer not to read the documentation and figure it out opn my own"
                author="Developer"
                containerStyle="mb-5"
              />
            </div>
          </div>
        </div>

        <SectionIntro text="Goals" />
        <div className="mt40 grid-main-container pb60">
          <div className="grid-container">
            <div className="grid grid-cols-1 md:grid-cols-2 lg:grid-cols-2 gap-5 md:gap-10 lg:gap-10">
              <div>
                <ul className="list-disc caseStudyContent">
                  <li className="hiddenScrollAnimation">
                    A user guide/ manual approach with as much less a
                    documentation as possible.
                  </li>
                  <li className="hiddenScrollAnimation">
                    Highly intuitive and user-friendly for both developers and
                    designers.
                  </li>
                  <li className="hiddenScrollAnimation">
                    Should be component/ library based with draggable components
                    so designers can quickly use it while implementing their
                    interface design.
                  </li>
                </ul>
                <div className="mt40">
                  <Image
                    className="w-full"
                    src="/images/drag-drop.gif"
                    height={300}
                    width={300}
                    alt="lo fi wireframes image"
                    style={{ objectFit: 'cover' }}
                  />
                </div>
              </div>
              <div>
                <Image
                  className="w-full"
                  src="/images/manual.gif"
                  height={300}
                  width={300}
                  alt="lo fi wireframes image"
                  style={{ objectFit: 'cover' }}
                  unoptimized
                />
              </div>
            </div>
          </div>
        </div>

        <div className="lightBg text-black pt60 pb60 mb60">
          <SectionIntro text="Understanding a design system" bg="light" />
          <div className="grid-main-container">
            <div className="grid-container">
              <ul className="pt-2.5 list-disc caseStudyContent">
                <li className="hiddenScrollAnimation">
                  <p className="mt40 uppercase text-sm font-semibold tracking-widest textPrimary hiddenScrollAnimation">
                    What is a design system?
                  </p>
                  By definition: A design system is a unified language that
                  helps a team solve problems consistently.
                </li>
              </ul>
            </div>
          </div>
          <div className="mt40 flex justify-center">
            <Image
              src="/images/understand-design-system.png"
              width={600}
              height={200}
              alt="lo fi wireframes image"
            />
          </div>
        </div>

        <SectionIntro text="Laying the foundation" />
        <div className="grid-main-container pt-2.5 ">
          <div className="pt-2.5 grid-container grid grid-cols-1 md:grid-cols-2 lg:grid-cols-2 gap-5 md:gap-10 lg:gap-10">
            <div>
              <p className="uppercase text-sm font-semibold tracking-widest textPrimary hiddenScrollAnimation">
                1. Component Collection
              </p>
              <p className="caseStudyContent hiddenScrollAnimation">
                Collected screenshots and made a list of the existing components
                that we need to add.
              </p>
            </div>

            <div>
              <p className="uppercase text-sm font-semibold tracking-widest textPrimary hiddenScrollAnimation">
                2. Component refinement
              </p>
              <p className="caseStudyContent hiddenScrollAnimation">
                Made a list of components that had room for improvements.
              </p>
            </div>
          </div>
          <div className="mt40 flex justify-center">
            <Image
              src="/images/foundation.png"
              width={1000}
              height={100}
              alt="lo fi wireframes image"
            />
          </div>

          <div className="pt60 pb60 grid-main-container">
            <div className="grid-container">
              <p className="uppercase textPrimary text-sm font-semibold  tracking-widest hiddenScrollAnimation">
                3. Brainstorming with developers to bridge knowledge gap.
              </p>
              <div className="pt-2.5 grid grid-cols-1 md:grid-cols-4 lg:grid-cols-4 gap-5 md:gap-10 lg:gap-10">
                <ul className="list-disc caseStudyContent col-span-1">
                  <li className="hiddenScrollAnimation">
                    Were they using any CSS library?
                  </li>
                  <li className="hiddenScrollAnimation">
                    To what extent can the components be customized?
                  </li>
                  <li className="hiddenScrollAnimation">
                    What is causing the numerous inconsistencies in the designs
                    throughout the interface?
                  </li>
                </ul>
                <div className="flex justify-center col-span-3">
                  <Image
                    src="/images/bridge.png"
                    width={700}
                    height={200}
                    alt="lo fi wireframes image"
                  />
                </div>
              </div>
            </div>
          </div>
        </div>

        <SectionIntro text="Our Limitations" />
        <div className="grid-main-container pt-2.5 pb60">
          <div className="pt-2.5 grid-container grid grid-cols-1 md:grid-cols-2 lg:grid-cols-2 gap-5 md:gap-10 lg:gap-10">
            <div>
              <p className="uppercase text-sm font-semibold tracking-widest textPrimary hiddenScrollAnimation">
                1. Small team
              </p>
              <p className="caseStudyContent hiddenScrollAnimation">
                The design team is limited to just a pair of designers
              </p>
            </div>

            <div>
              <p className="uppercase text-sm font-semibold tracking-widest textPrimary hiddenScrollAnimation">
                2. A Mature Product in Action
              </p>
              <p className="caseStudyContent hiddenScrollAnimation">
                The product had already been built, matured, and was being
                successfully used by various clients.
              </p>
            </div>

            <div>
              <p className="uppercase text-sm font-semibold tracking-widest textPrimary hiddenScrollAnimation">
                3. Managing Side Project Constraints
              </p>
              <p className="caseStudyContent hiddenScrollAnimation">
                Initially, the project was more like a side-project, and we
                couldn't allocate our full time to it.
              </p>
            </div>
          </div>
        </div>

        <SectionIntro text="How are we going to reach or goals?" />
        <div className="mt40 grid-main-container pb60">
          <div className="grid-container">
            <p className="uppercase text-sm font-semibold tracking-widest textPrimary hiddenScrollAnimation">
              Process followed
            </p>
            <ul className="list-disc caseStudyContent">
              <li className="hiddenScrollAnimation">
                We collaborated and came up with three steps to be followed.
              </li>
            </ul>
            <div className="mt40 flex justify-center">
              <Image
                src="/images/ds-3.png"
                width={1000}
                height={100}
                alt="lo fi wireframes image"
              />
            </div>
          </div>
        </div>

        <div className="grid-container">
          <div className="mt40 grid-main-container pb60">
            <p className="mt40 uppercase text-sm font-semibold tracking-widest textPrimary hiddenScrollAnimation">
              Approach: Atom, molecules approach
            </p>
            <div className="grid grid-cols-1 md:grid-cols-2 lg:grid-cols-2 gap-5 md:gap-10 lg:gap-10">
              <div>
                <ul className="list-disc caseStudyContent">
                  <li className="hiddenScrollAnimation">
                    We started exploring the various Design System in the market
                    - we ended up with utilizing the{' '}
                    <a
                      href="https://atomicdesign.bradfrost.com/chapter-2/#:~:text=Atomic%20design%20is%20atoms%2C%20molecules,parts%20at%20the%20same%20time"
                      target="_blank"
                      className="underline textPrimary"
                    >
                      Atomic design by Brad frost method.
                    </a>
                  </li>
                </ul>
                <div className="mt40 flex justify-center">
                  <Image
                    src="/images/atom-1.png"
                    width={400}
                    height={100}
                    alt="lo fi wireframes image"
                  />
                </div>
              </div>
              <ul className="list-disc caseStudyContent">
                <li className="hiddenScrollAnimation">
                  In this method, Brad frost talks about how a Design System can
                  be compared to chemical equations.
                </li>
                <li className="hiddenScrollAnimation">
                  In the example, we see how hydrogen and oxygen combine to form
                  water molecules.
                </li>
              </ul>
            </div>
            <div className="mt40 flex justify-center">
              <Image
                src="/images/atom-2.png"
                width={700}
                height={200}
                alt="lo fi wireframes image"
              />
            </div>
          </div>
        </div>

        <div className="bg-dark text-white pt60 pb60">
          <SectionIntro text="Defining the basic elements - Atoms " bg="dark" />
          <div className="grid-main-container">
            <div className="grid-container">
              <p className="mt40 uppercase text-sm font-semibold  tracking-widest hiddenScrollAnimation">
                Colors
              </p>
              <ul className="list-disc caseStudyContent">
                <li className="hiddenScrollAnimation">
                  We started the ds with defining the atomic element - color
                </li>
                <li className="hiddenScrollAnimation">Base shade as 50</li>
                <li className="hiddenScrollAnimation">
                  5 shades variation of primary and semantic colors - 2 shades
                  lower than base and 2 shades darker.
                </li>
              </ul>
              <div className="mt40 flex justify-center">
                <Image
                  src="/images/color1a.gif"
                  width={1000}
                  height={100}
                  alt="lo fi wireframes image"
                />
              </div>

              <p className="mt40 uppercase text-sm font-semibold bg-light tracking-widest hiddenScrollAnimation">
                Primary and secondary colors
              </p>
              <div className="grid grid-cols-1 md:grid-cols-2 lg:grid-cols-2 gap-5">
                <ul className="list-disc caseStudyContent">
                  <li className="hiddenScrollAnimation">
                    Usually the shade derived from the brand.
                  </li>
                </ul>
                <LinkBox
                  url="https://www.getgreenr.org/"
                  text="Client's project logo"
                  containerStyleClass="w-fit"
                />
              </div>
              <div className="mt40 grid-container lightBg py-4 grid grid-cols-1 md:grid-cols-2 lg:grid-cols-2 gap-5">
                <div className="flex justify-end">
                  <Image
                    src="/images/atoms-colors-1.png"
                    width={400}
                    height={200}
                    alt="lo fi wireframes image"
                  />
                </div>
                <div className="flex justify-start">
                  <Image
                    src="/images/atoms-colors-2.png"
                    width={400}
                    height={200}
                    alt="lo fi wireframes image"
                  />
                </div>
              </div>
            </div>
          </div>
        </div>

        <div className=" pb60 grid-main-container">
          <div className="grid-container">
            <p className="uppercase textPrimary text-sm font-semibold  tracking-widest hiddenScrollAnimation">
              Semantic colors
            </p>
            <div className="pt-2.5 grid grid-cols-1 md:grid-cols-4 lg:grid-cols-4 gap-5 md:gap-10 lg:gap-10">
              <ul className="list-disc caseStudyContent col-span-1">
                <li className="hiddenScrollAnimation">Info</li>
                <li className="hiddenScrollAnimation">Success</li>
                <li className="hiddenScrollAnimation">Warning</li>
                <li className="hiddenScrollAnimation">Error</li>
              </ul>
              <div className="flex justify-center col-span-3">
                <Image
                  src="/images/semantic-1.png"
                  width={700}
                  height={200}
                  alt="lo fi wireframes image"
                />
              </div>
            </div>
            <p className="mt40 textPrimary uppercase text-sm font-semibold tracking-widest hiddenScrollAnimation">
              Neutral color palette
            </p>
            <ul className="pt-2.5 list-disc caseStudyContent">
              <li className="hiddenScrollAnimation">
                Starting from solid white to solid black.
              </li>
              <li className="hiddenScrollAnimation">
                Any other replacement of solid black - some designers don't
                prefer to use solid black as it can seem very harsh.
              </li>
              <li className="hiddenScrollAnimation">
                Mostly more shades than 5
              </li>
            </ul>
            <div className="mt40 flex justify-center">
              <Image
                src="/images/neutral-colors-1.png"
                width={700}
                height={200}
                alt="lo fi wireframes image"
              />
            </div>
            <p className="mt40 textPrimary text-sm font-semibold  uppercase tracking-widest hiddenScrollAnimation">
              Colors in design system
            </p>
            <div className="pt-2.5 flex justify-center">
              <Image
                className=""
                src="/images/neutral-colors-2.png"
                width={800}
                height={200}
                alt="lo fi wireframes image"
              />
            </div>
          </div>
        </div>

        <div className="bg-dark text-white pb60">
          <div className="grid-main-container">
            <div className="grid-container">
              <p className=" uppercase text-sm font-semibold  tracking-widest hiddenScrollAnimation">
                Variable Level Implementation
              </p>
              <ul className="list-disc caseStudyContent">
                <li className="hiddenScrollAnimation">
                  Used Figma to its full potential, implemted variables for
                  different products in white label.
                </li>
              </ul>

              <div className="mt40 flex justify-center">
                <Image
                  src="/images/color1b.gif"
                  width={1000}
                  height={100}
                  alt="lo fi wireframes image"
                />
              </div>
            </div>
          </div>
        </div>

        <div className="pt60 pb60 grid-main-container">
          <div className="grid-container">
            <p className="uppercase textPrimary text-sm font-semibold  tracking-widest hiddenScrollAnimation">
              Swatching and Documenting existing Colors
            </p>
            <div className="pt-2.5 grid grid-cols-1 md:grid-cols-4 lg:grid-cols-4 gap-5 md:gap-10 lg:gap-10">
              <ul className="list-disc caseStudyContent col-span-1">
                <li className="hiddenScrollAnimation">
                  {' '}
                  We Reduced and Mapped existing colors in code base to avaoid
                  confusion
                </li>
              </ul>
              <div className="flex justify-center col-span-3">
                <Image
                  src="/images/color1c.png"
                  width={700}
                  height={200}
                  alt="lo fi wireframes image"
                />
              </div>
            </div>
          </div>
        </div>

        <div className="lightBg text-black pt60 pb60">
          <SectionIntro text="Spacing and grids" bg="light" />
          <div className="grid-container">
            <div className="grid-main-container">
              <ul className="list-disc caseStudyContent">
                <li className="hiddenScrollAnimation">
                  Its the negative area between elements and components.
                </li>
              </ul>
              <div className="mt40 flex justify-center">
                <Image
                  className="w-full"
                  src="/images/spacings.png"
                  width={900}
                  height={200}
                  alt="lo fi wireframes image"
                />
              </div>
            </div>
          </div>
        </div>
        <div className="pt60 textPrimary pb60 grid-main-container">
          <SectionIntro text="following a grid system" />
          <div className="grid-container">
            <ul className="pt-2.5 list-disc caseStudyContent">
              <li className="hiddenScrollAnimation">
                We decided to follow the 8px grid system and defined the tokens
                for it along with the specs.
              </li>
            </ul>
            <div className="mt40 flex justify-center">
              <Image
                className="w-full"
                src="/images/spacing-2.png"
                width={1000}
                height={200}
                alt="lo fi wireframes image"
              />
            </div>
          </div>
        </div>
        <div className="lightBg text-black pt60 pb60">
          <SectionIntro text="Typography" bg="light" />
          <div className="grid-container">
            <div className="grid-main-container">
              <ul className="list-disc caseStudyContent">
                <li className="hiddenScrollAnimation">
                  Defining a typography scale beforehand always makes the
                  process much easier, the line height of the typography always
                  matters for the height of the elements such as buttons,
                  inputs, etc.
                </li>
              </ul>
              <div className="mt40 flex justify-center">
                <Image
                  className=""
                  src="/images/ds-typo-1.png"
                  width={800}
                  height={100}
                  alt="lo fi wireframes image"
                />
              </div>
            </div>
          </div>
        </div>

        <SectionIntro
          containerStyleClass="mt60"
          text="Designing the Bases for Components"
        />
        <div className="grid-container">
          <div className="grid-main-container">
            <ul className="list-disc caseStudyContent">
              <li className="hiddenScrollAnimation">
                Designed a reuseable dynamic bases for change Implementation
              </li>
            </ul>
            <div className="mt40 flex justify-center">
              <Image
                className=""
                src="/images/base.gif"
                width={800}
                height={100}
                alt="lo fi wireframes image"
              />
            </div>

            <p className="mt40 textPrimary uppercase text-sm font-semibold tracking-widest hiddenScrollAnimation">
              Bases for other components (Checkbox, Radio, etc)
            </p>
            <div className="mt40 flex justify-center">
              <Image
                className=""
                src="/images/base1.png"
                width={800}
                height={100}
                alt="lo fi wireframes image"
              />
            </div>
          </div>
        </div>

        <SectionIntro containerStyleClass="mt60" text="Buttons" />
        <div className="mt40 grid-main-container pb60">
          <div className="grid-container">
            <p className="mt40 textPrimary uppercase text-sm font-semibold tracking-widest hiddenScrollAnimation">
              Designing the button
            </p>
            <div className="pt-2.5 grid grid-cols-1 md:grid-cols-2 lg:grid-cols-2 gap-5 md:gap-10 lg:gap-10">
              <div className="flex justify-center">
                <Image
                  className="w-full"
                  src="/images/buttons-1.png"
                  width={200}
                  height={200}
                  alt="lo fi wireframes image"
                  unoptimized
                />
              </div>
              <ul className="list-disc caseStudyContent">
                <li className="hiddenScrollAnimation">
                  We collaborated as a team to define the states, types, sizes
                  and specs of the buttons.
                </li>
                <li className="hiddenScrollAnimation">
                  We used freeform for all our initial discussions and
                  brainstorming for the low-fi wire-framing
                </li>
              </ul>
            </div>
          </div>
        </div>
        <div className="lightBg text-black pt60 pb60">
          <div className="grid-main-container">
            <div className="grid-container">
              <p className="uppercase text-sm font-semibold  tracking-widest hiddenScrollAnimation">
                Final button component
              </p>
              <div className="pt-2.5 grid grid-cols-1 md:grid-cols-2 lg:grid-cols-2 gap-5">
                <div className="flex justify-center">
                  <Image
                    className="w-full"
                    src="/images/button-interaction.gif"
                    width={400}
                    height={200}
                    alt="lo fi wireframes image"
                  />
                </div>
                <div className="flex justify-center">
                  <Image
                    className="w-full"
                    src="/images/button-size-icon.gif"
                    width={400}
                    height={200}
                    alt="lo fi wireframes image"
                  />
                </div>
              </div>
              <p className="mt40 uppercase text-sm font-semibold  tracking-widest hiddenScrollAnimation">
                Button dimensions
              </p>
              <div className="pt-2.5">
                <Image
                  className="w-full"
                  src="/images/button-dim.png"
                  width={200}
                  height={200}
                  alt="lo fi wireframes image"
                  unoptimized
                />
              </div>
              <p className="mt40 uppercase text-sm font-semibold  tracking-widest hiddenScrollAnimation">
                Button in design system
              </p>
              <div className="pt-2.5">
                <Image
                  className="w-full"
                  src="/images/ds-button-2.png"
                  width={400}
                  height={200}
                  alt="lo fi wireframes image"
                  unoptimized
                />
              </div>
            </div>
          </div>
        </div>
        <SectionIntro
          containerStyleClass="mt60"
          text="Designing the molecules"
        />
        <div className="grid-main-container pb60">
          <div className="grid-container">
            <ul className=" pt-2.5 list-disc caseStudyContent">
              <li className="hiddenScrollAnimation">
                Molecules: A combination of atoms.
              </li>
              <li className="hiddenScrollAnimation">
                Input element combined with a button.
              </li>
            </ul>
            <div className="mt40 grid grid-cols-1 md:grid-cols-3 lg:grid-cols-3 gap-5">
              <div className="flex justify-center col-span-2">
                <Image
                  src="/images/mol-1.png"
                  width={900}
                  height={200}
                  alt="lo fi wireframes image"
                  style={{ objectFit: 'cover' }}
                />
              </div>
              <div className="flex justify-center">
                <Image
                  className="w-full"
                  src="/images/molecule.gif"
                  width={300}
                  height={200}
                  alt="lo fi wireframes image"
                  style={{ objectFit: 'cover' }}
                />
              </div>
            </div>
          </div>
        </div>
        <div className="lightBg text-black pt60 pb60">
          <div className="grid-main-container">
            <div className="grid-container">
              <p className="uppercase text-sm font-semibold tracking-widest hiddenScrollAnimation">
                Molecules in design system
              </p>
              <div className=" pt-2.5 flex justify-center">
                <Image
                  className="w-full"
                  src="/images/mol-in-ds.png"
                  width={400}
                  height={200}
                  alt="lo fi wireframes image"
                  unoptimized
                />
              </div>
            </div>
          </div>
        </div>

        <SectionIntro containerStyleClass="pt60" text="Lessons Learnt" />
        <div className="grid-main-container pt-2.5 pb60">
          <div className="pt-2.5 grid-container grid grid-cols-1 md:grid-cols-2 lg:grid-cols-2 gap-5 md:gap-10 lg:gap-10">
            <div>
              <p className="uppercase text-sm font-semibold tracking-widest textPrimary hiddenScrollAnimation">
                1. Timely Update
              </p>
              <p className="caseStudyContent hiddenScrollAnimation">
                Design system needs to be constantly updated and evolved along
                with the product and team.
              </p>
            </div>
            <div>
              <p className="uppercase text-sm font-semibold tracking-widest textPrimary hiddenScrollAnimation">
                2. Purpose Boomerang
              </p>
              <p className="caseStudyContent hiddenScrollAnimation">
                The purpose of Design system can actually backfire if it is not
                updated regularly.
              </p>
            </div>
            <div>
              <p className="uppercase text-sm font-semibold tracking-widest textPrimary hiddenScrollAnimation">
                3. Easily Forgotten
              </p>
              <p className="caseStudyContent hiddenScrollAnimation">
                Design system can be easily forgotten and tossed into the trash
                if not maintained
              </p>
            </div>
            <div>
              <p className="uppercase text-sm font-semibold tracking-widest textPrimary hiddenScrollAnimation">
                4. Easy Management
              </p>
              <p className="caseStudyContent hiddenScrollAnimation">
                Always have an easy mechanism to manage the Design system.
              </p>
            </div>
          </div>
        </div>

        <SectionIntro
          containerStyleClass="mt60"
          text="maintaining a design system"
        />
        <div className="grid-main-container mt40 pb60">
          <div className="grid-container">
            <div className="mt40">
              <p className="uppercase text-sm font-semibold tracking-widest textPrimary hiddenScrollAnimation">
                1. Make a list of components need to be added to the design
                system
              </p>
              <ul className="list-disc caseStudyContent">
                <li className="hiddenScrollAnimation">
                  It could be as simple as an Excel sheet with the name and
                  status of the component, a notion template or in your Figma
                  file
                </li>
              </ul>
              <div className="mt40 flex justify-center">
                <Image
                  className=""
                  src="/images/addedComp.png"
                  width={1000}
                  height={200}
                  alt="lo fi wireframes image"
                />
              </div>
            </div>
            <div className="mt60 grid grid-cols-1 md:grid-cols-2 lg:grid-cols-2 gap-5 md:gap-10 lg:gap-10">
              <div>
                <div>
                  <p className="uppercase text-sm font-semibold tracking-widest textPrimary hiddenScrollAnimation">
                    2. Components that have room for improvement
                  </p>
                  <ul className="list-disc caseStudyContent">
                    <li className="hiddenScrollAnimation">
                      A list of improvements or suggestions for the existing
                      components.
                    </li>
                  </ul>
                </div>
                <div className="mt40">
                  <p className="uppercase text-sm font-semibold tracking-widest textPrimary hiddenScrollAnimation">
                    3. Have a design audit frequently
                  </p>
                  <ul className="list-disc caseStudyContent">
                    <li className="hiddenScrollAnimation">
                      Most likely some components in the design system would be
                      outdated and will be recognized during the audit process.
                    </li>
                  </ul>
                </div>
              </div>
              <div className="flex justify-center">
                <Image
                  className="w-full"
                  src="/images/improveComp.png"
                  width={400}
                  height={200}
                  alt="lo fi wireframes image"
                  unoptimized
                />
              </div>
            </div>

            <div className="mt40">
              <p className="uppercase text-sm font-semibold tracking-widest textPrimary hiddenScrollAnimation">
                4. Updated component - Compare with old
              </p>
              <ul className="list-disc caseStudyContent">
                <li className="hiddenScrollAnimation">
                  If any component is updated in the system, always make sure to
                  document.
                </li>
                <li className="hiddenScrollAnimation">
                  Compare it with the older one, so as to indicate which
                  component has been updated.
                </li>
              </ul>
              <div className="mt40 flex justify-center">
                <Image
                  className="w-full"
                  src="/images/ds-mnt-2.png"
                  width={400}
                  height={200}
                  alt="lo fi wireframes image"
                  unoptimized
                />
              </div>
            </div>

            <div className="mt40">
              <p className="uppercase text-sm font-semibold tracking-widest textPrimary hiddenScrollAnimation">
                5. Use of status tag
              </p>
              <div className="pt-2.5 grid grid-cols-1 md:grid-cols-2 lg:grid-cols-2 gap-5 md:gap-10 lg:gap-10">
                <ul className="list-disc caseStudyContent">
                  <li className="hiddenScrollAnimation">
                    We use status tags to indicate if the component is under
                    constructions so that the user viewing or planning to use it
                    is aware.
                  </li>
                </ul>
                <div className="flex justify-center">
                  <Image
                    className="w-full"
                    src="/images/ds-mnt-3.png"
                    width={400}
                    height={200}
                    alt="lo fi wireframes image"
                  />
                </div>
              </div>
            </div>
            <div className="mt40 flex justify-center">
              <Image
                className="w-full"
                src="/images/ds-mnt-4.png"
                width={400}
                height={200}
                alt="lo fi wireframes image"
                unoptimized
              />
            </div>
          </div>
        </div>
        <SectionIntro text="Takeaway" />
        <div className="grid-main-container pt-2.5 pb60">
          <div className="pt-2.5 grid-container grid grid-cols-1 md:grid-cols-2 lg:grid-cols-2 gap-5 md:gap-10 lg:gap-10">
            <div>
              <p className="uppercase text-sm font-semibold tracking-widest textPrimary hiddenScrollAnimation">
                1. Design system: A never-ending process
              </p>
              <p className="caseStudyContent hiddenScrollAnimation">
                As the product keeps on evolving, the components in the design
                system needs to be improved.
              </p>
            </div>
            <div>
              <p className="uppercase text-sm font-semibold tracking-widest textPrimary hiddenScrollAnimation">
                2. Planning Ahead: System for maintenance of design system
              </p>
              <p className="caseStudyContent hiddenScrollAnimation">
                Developing a design system requires significant attention to its
                maintenance process. <br></br>Continuous upkeep is vital to its
                success and longevity.
              </p>
            </div>
            <div>
              <p className="uppercase text-sm font-semibold tracking-widest textPrimary hiddenScrollAnimation">
                3. The power of collaboration
              </p>
              <p className="caseStudyContent hiddenScrollAnimation">
                The gap in knowledge can only be reduced by communicating and
                collaborating, constant collaboration between the product,
                business, development and design teams is required.
              </p>
            </div>
            <div>
              <p className="uppercase text-sm font-semibold tracking-widest textPrimary hiddenScrollAnimation">
                4. Get to know your users
              </p>
              <p className="caseStudyContent hiddenScrollAnimation">
                Users of the Design system are usually the developers and
                designers from other teams. <br></br>The makers are the major
                users.
              </p>
            </div>
          </div>
        </div>
        <SectionIntro text="Impact" />
        <div className="grid-main-container pb60">
          <div className="grid-container">
            <ul className=" pt-2.5 list-disc caseStudyContent">
              <li className="hiddenScrollAnimation">
                This design system structure and the process were followed in
                multiple projects.
              </li>
              <li className="hiddenScrollAnimation">
                Shared among the other design teams.
              </li>
              <li className="hiddenScrollAnimation">
                Raved internally among the dev and product teams.
              </li>
            </ul>
            <div className="mt40 flex justify-center">
              <Image
                className="w-full"
                src="/images/impact-ds.png"
                width={400}
                height={200}
                alt="lo fi wireframes image"
                unoptimized
              />
            </div>
            <div className="mt40">
              <p className="uppercase text-sm font-semibold tracking-widest textPrimary hiddenScrollAnimation">
                1. Product and team
              </p>
              <ul className="list-disc caseStudyContent">
                <li className="hiddenScrollAnimation">
                  My team really loved and was very happy to have a systematic
                  approach to the constant design changes.
                </li>
                <li className="hiddenScrollAnimation">
                  We had this common blueprint that any of us could refer to
                  whenever we were stuck in any situation, and showcase each
                  other where they might have misunderstood.
                </li>
              </ul>
            </div>
            <div className="mt40">
              <p className="uppercase text-sm font-semibold tracking-widest textPrimary hiddenScrollAnimation">
                2. Designers
              </p>
              <ul className="list-disc caseStudyContent">
                <li className="hiddenScrollAnimation">
                  Designers were relieved that they were not constantly bugged
                  by others for minor collaboration regarding the scales of
                  dimensions.
                </li>
                <li className="hiddenScrollAnimation">
                  Also, for new designers joining the team, it worked quite well
                  in understanding the design language and methodology.
                </li>
              </ul>
            </div>
            <div className="mt40">
              <p className="uppercase text-sm font-semibold tracking-widest textPrimary hiddenScrollAnimation">
                3. developers
              </p>
              <ul className="list-disc caseStudyContent">
                <li className="hiddenScrollAnimation">
                  Devs were overjoyed that they don't have to worry about the
                  design not matching after development during the design review
                  stage.
                </li>
              </ul>
            </div>
          </div>
        </div>
        <SectionIntro text="Future scope" />
        <div className="grid-main-container pt-2.5 pb40 pb60">
          <div className="grid-container">
            <ul className="list-disc caseStudyContent">
              <li className="hiddenScrollAnimation">
                Addition of more documentation.
              </li>
              <li className="hiddenScrollAnimation">
                Release a library online with code snippets.
              </li>
              <li className="hiddenScrollAnimation">
                Probably have a separate team dedicated to the design system.
              </li>
            </ul>
          </div>
        </div>
        <Disclaimer disclaimerContent={designSystemDisclaimer} />
      </div>
    </>
  );
};

export default DesignSystem;
