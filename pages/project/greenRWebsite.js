import Image from 'next/image';
import SectionIntro from '../../components/sectionIntro/sectionIntro';
import Heading from '../../components/shared/heading/heading';
import Title from '../../components/title/title';
import Quote from '../../components/quote/quote';
import LinkBox from '../../components/linkBox/linkBox';
import { Carousel } from 'flowbite-react';
import Disclaimer from '../../components/disclaimer/disclaimer';
import { useEffect } from 'react';
import animation from '../../utils/animation';
import SubTitle from '../../components/shared/subTitle/subTitle';

const GreenRWebsite = () => {
  useEffect(() => {
    animation.afterCallback(
      new IntersectionObserver(
        animation.instersectioOberserverCallback,
        animation.rootMargin
      )
    );
  }, []);

  const stats = [
    { tag: 'Role', text: 'UI Designer and Frontend Developer' },
    { tag: 'Duration', text: '9 Weeks' },
    { tag: 'Tools', text: 'Figma, Next.js, Tailwind CSS' },
  ];

  const teamMembers = [
    { name: 'Quba Abdus', role: 'UI Designer and Frontend Developer' },
    { name: 'Akshata Kenjale', role: 'Lead UX Designer | Manager' },
    { name: 'Manisha Garje', role: 'Graphic Designer' },
    { name: 'Ravendra Prajapati', role: 'Backend Developer' },
  ];

  const greenRDisclaimer = `<p className="textGrey">
  ~ All information expressed in this case study is my own and does not reflect the views of any organization.
</p>
<p className="textGrey">
please do not share or upload the content or images from this case study without consent
</p>
<p className="textGrey">
i do not own the images and icons used in this case study, credit to the owners and respective clients.
</p>`;

  const carouselImageNames = [
    'carousel-1',
    'carousel-2',
    'carousel-3',
    'carousel-4',
    'carousel-5',
    'carousel-6',
    'carousel-7',
    'carousel-8',
  ];

  return (
    <>
      <div className="darkBgContainer">
        <div className="grid-main-container text-center projectPageIntro">
          <div className="grid-container">
            <Heading text={`Green<span className="pinkText">R</span>`} />
            <SubTitle
              text="A visually captivating website that enables start-ups to attain sustainability across environmental, social, and commercial aspects."
              styleType="normal"
              styleClass="pt-3 caseStudyContent hiddenScrollAnimation hiddenHeadingAnimation w-[100%] md:w-[70%] mx-auto"
            />
            <p className="textPrimary py-5 hiddenScrollAnimation">
              A Case Study
            </p>
            <p className="uppercase tracking-widest hiddenScrollAnimation">
              Front-end dev & Ui Designer | client project
            </p>
          </div>
        </div>

        <div className="lightBg text-black pt60 pb60">
          <div className="grid-main-container">
            <div className="grid-container grid grid-cols-1 md:grid-cols-2 lg:grid-cols-2 gap-5 md:gap-10 lg:gap-10">
              <div>
                {stats.map((v, i) => (
                  <div
                    key={`key_stats_${i}`}
                    className={i === stats.length - 1 ? '' : 'mb10'}
                  >
                    <span className="font-bold mr-2 hiddenScrollAnimation">
                      {v.tag}
                    </span>
                    <p className="inline hiddenScrollAnimation">{v.text}</p>
                  </div>
                ))}
              </div>
              <div>
                <span className="font-bold mb10 block hiddenScrollAnimation">
                  Team
                </span>
                {teamMembers.map((v, i) => (
                  <div
                    key={`key_teamMembers_${i}`}
                    className={i === teamMembers.length - 1 ? '' : 'mb10'}
                  >
                    <p className="font-bold hiddenScrollAnimation">{v.name}</p>
                    <p className="hiddenScrollAnimation">{v.role}</p>
                  </div>
                ))}
              </div>
            </div>
          </div>
        </div>

        <SectionIntro containerStyleClass="mt60" text="overview" />
        <div className="grid-main-container pt-2.5">
          <div className="grid-container">
            <p className="caseStudyContent pb60 hiddenScrollAnimation">
              GreenR helps transform passion into revenue and innovators into
              CEOs by providing programs which guides and advices business to
              make them more sustainable — environmentally, socially, and
              commercially.
            </p>
            <div className="hidden md:block lg:block pb40 widthWrap">
              <div className="h-[100%] relative">
                <Image
                  src="/images/intro-new.gif"
                  fill
                  alt="lo fi wireframes image"
                  unoptimized
                />
              </div>
            </div>
          </div>
        </div>
        <div className="lightBg text-black pt60 pb60">
          <SectionIntro text="my role" bg="light" />
          <div className="grid-main-container pt-2.5">
            <div className="grid-container grid grid-cols-1 md:grid-cols-2 lg:grid-cols-2 gap-5 md:gap-10 lg:gap-10">
              <p className="caseStudyContent hiddenScrollAnimation">
                I worked collaboratively with the lead designer Akshata as a UI
                designer and a frontend developer. As a designer I was
                responsible for making the client's vision into reality by
                creating prototypes with the required layout and visual designs.
              </p>
              <p className="caseStudyContent hiddenScrollAnimation">
                Along with being intensively involved with the design, I was
                also responsible for the front-end development of the design
                layouts into actual interactive interfaces using next.js and
                tailwind.css.
                <br />
                Being involved with both design and coding helped with my design
                decision-making and really enhanced my coding skills.
              </p>
            </div>
          </div>
        </div>

        <SectionIntro
          containerStyleClass="mt60"
          text="timeline including the developing phase"
        />
        <div className="grid-main-container mt-2.5">
          <div className="grid-container">
            <ul className="list-disc caseStudyContent">
              <li className="hiddenScrollAnimation">
                In the course of 9 weeks, we started the first week with
                brainstorming, research and wireframing sessions along with the
                client.
              </li>
              <li className="hiddenScrollAnimation">
                Over the next 6 weeks design and development was done
                simultaneously in which we had 2 phase releases. Before
                development the design lead akshata gave us design feedback and
                critiques and the designs were also approved by the client.
              </li>
              <li className="hiddenScrollAnimation">
                After development each release was reviewed by the client and
                all the received feedback and edits were done in a timeline of 1
                week. We wrapped up the project in the last week of january with
                the final testing.
              </li>
            </ul>
          </div>
        </div>

        <SectionIntro containerStyleClass="mt60" text="project brief" />
        <div className="grid-main-container mt-2.5 pb60">
          <div className="grid-container">
            <ul className="list-disc caseStudyContent">
              <li className="hiddenScrollAnimation">
                Greenr is an initiative which helps businesses to make them more
                sustainable.
              </li>
              <li className="hiddenScrollAnimation">
                They provide programs which are 12 to 21 months long which
                provide advice and tests regarding the green consumption.
              </li>
              <li className="hiddenScrollAnimation">
                They help to identify and execute strategies which are needed to
                build revenue models with most promising environment action
                entrepreneurs.
              </li>
              <li className="hiddenScrollAnimation">
                The aim of greenr is to restore the environment and uplift
                communities around the world through scale-bound startups.
              </li>
              <li className="hiddenScrollAnimation">
                The programs are supported by Intuit, Moody's Corporation,
                Goldman Sachs, the Blackstone Foundation and others.
              </li>
            </ul>
          </div>
        </div>

        <div className="lightBg text-black pt60 pb60">
          <SectionIntro text="lo fi wireframes" bg="light" />
          <div className="mt40 h-[400px] md:h-[715px] lg:h-[715px] relative">
            <Image
              src="/images/lo-fi-wireframes.png"
              fill
              alt="lo fi wireframes image"
              style={{ objectFit: 'cover' }}
            />
          </div>
          <div className="grid-main-container">
            <div className="grid-container">
              <p className="mt40 w-[80%] hiddenScrollAnimation">
                As I was primarily focused on the visual/UI designs and frontend
                development for this project, the ux and wireframes were given
                to me by our lead ux designer Akshata. My job here was to bring
                these wireframes into live with the expected branding, visual
                elements and design languages.
              </p>
            </div>
          </div>

          <SectionIntro
            containerStyleClass="mt60 pb40"
            text="logo design"
            bg="light"
          />
          <div className="grid-main-container">
            <div className="grid-container ">
              <div className="grid grid-cols-1 md:grid-cols-2 lg:grid-cols-2 gap-5 md:gap-10 lg:gap-10">
                <div className="h-[233px] md:h-[446px] lg:h-[446px] relative">
                  <Image
                    src="/images/logo-design-1.png"
                    fill
                    alt="lo fi wireframes image"
                    style={{ objectFit: 'cover' }}
                  />
                </div>
                <div className="h-[233px] md:h-[446px] lg:h-[446px] relative">
                  <Image
                    src="/images/logo-design-2.png"
                    fill
                    alt="lo fi wireframes image"
                    style={{ objectFit: 'cover' }}
                  />
                </div>
              </div>
              <p className="mt40 w-[80%] hiddenScrollAnimation">
                We had a talented logo designer Manisha, who did an absolutely
                amazing job in creating the fun and meaningful logo.
              </p>
              <div className="mt40 flex justify-center">
                <Image
                  src="/images/logo-design-3.png"
                  width={810}
                  height={250}
                  alt="lo fi wireframes image"
                />
              </div>

              <p className="mt40 w-[80%] hiddenScrollAnimation">
                Collaborating with her to make sure that the logo design
                language is in alignment of the interface designs was my job
                here.
              </p>
              <div className="mt40 flex justify-center">
                <Image
                  src="/images/logo-design-4.png"
                  width={788}
                  height={496}
                  alt="lo fi wireframes image"
                />
              </div>
            </div>
          </div>
        </div>

        <SectionIntro containerStyleClass="mt60 pb40" text="Design Process" />
        <div className="grid-main-container pb60">
          <div className="grid-container">
            <h3 className="uppercase text-sm font-semibold tracking-widest textPrimary pb10 hiddenScrollAnimation">
              User's needs
            </h3>
            <div className="grid grid-cols-1 md:grid-cols-4 lg:grid-cols-4 gap-5 md:gap-10 lg:gap-10">
              <ul className="list-disc  caseStudyContent col-span-4 md:col-span-2 lg:col-span-2">
                <li className="hiddenScrollAnimation">
                  The design language expectations from the client was pretty
                  much - minimal but unique in feature.
                </li>
                <li className="hiddenScrollAnimation">
                  Each component was expected to be unique in its own way, while
                  maintaining consistency and simplicity.
                </li>
              </ul>
              <ul className="list-disc caseStudyContent col-span-4 md:col-span-2 lg:col-span-2">
                <li className="hiddenScrollAnimation">
                  Designing for our users i also kept in my mind the client's
                  requirements since theses requirements directly reflected the
                  user's requirements backed up with the tremendous research.
                </li>
              </ul>
              <Quote
                containerStyle="col-span-2 col-start-2"
                text="I would suggest that before the next design comes in, the team also spends some time on incorporating more modern aesthetics i nto it. Minimal, with interesting detailing works for us. It's an easier aesthetic to work with."
                author="client"
              />
            </div>
          </div>
        </div>

        <div className="lightBg text-black pt60">
          <SectionIntro
            containerStyleClass="pb-2.5"
            text="Components"
            bg="light"
          />
          <div className="grid-main-container">
            <div className="grid-container">
              <ul className="list-disc caseStudyContent pb40">
                <li className="hiddenScrollAnimation">
                  The components were expected to be uniquely designed.
                  Meaningful, visually appealing with use of geometric shapes
                  with subtle animations.
                </li>
                <li className="hiddenScrollAnimation">
                  Experimented with various variations.
                </li>
              </ul>
              <div className="widthWrap">
                <div className="h-[100%]">
                  <Carousel className="">
                    {carouselImageNames.map((v, i) => (
                      <Image
                        key={`key_carousel_imgs_${i}`}
                        src={`/images/${v}.png`}
                        width={837}
                        height={530}
                        alt={v}
                        style={{ objectFit: 'cover' }}
                      />
                    ))}
                  </Carousel>
                </div>
              </div>
            </div>
          </div>

          <SectionIntro
            containerStyleClass="mt60 pt40 pb-2.5"
            text="Components exploration"
            bg="light"
          />
          <div className="grid-main-container">
            <div className="grid-container ">
              <ul className="list-disc caseStudyContent pb40">
                <li className="hiddenScrollAnimation">
                  Experimented with various variations.
                </li>
              </ul>
            </div>
          </div>
          <div className="h-[400px] md:h-[800px] lg:h-[800px] relative">
            components-2.png
            <Image
              src="/images/components-2.png"
              fill
              alt="lo fi wireframes image"
              style={{ objectFit: 'cover' }}
            />
          </div>
        </div>

        <SectionIntro
          containerStyleClass="mt60 pb40"
          text="design exploration"
        />
        <div className="grid-main-container pb60">
          <div className="grid-container">
            <h3 className="uppercase text-sm font-semibold tracking-widest textPrimary pb10 hiddenScrollAnimation">
              colors
            </h3>
            <div className="grid grid-cols-1 md:grid-cols-2 lg:grid-cols-2 gap-5 md:gap-10 lg:gap-10">
              <ul className="list-disc caseStudyContent">
                <li className="hiddenScrollAnimation">
                  Since we are dealing with nature, earthy tones were expected.
                </li>
                <li className="hiddenScrollAnimation">
                  The client really wanted to visually explore different
                  possible color options so we as designers decided to show
                  multiple variations of color pallette implemented into the
                  landing pages to give a sense of the visuals.
                </li>
              </ul>
              <Quote
                text="Can we explore some fresh colours that complement the green? I resonate with the recall that green brings as representative of climate action, but for the other two colours we can explore other options. To help us to create a something a more unique visual identity."
                author="client"
              />
            </div>
            <div className="mt40 h-[400px] md:h-[800px] lg:h-[800px] relative">
              <Image
                src="/images/design-1.png"
                alt="lo fi wireframes image"
                fill
                style={{ objectFit: 'cover' }}
              />
            </div>
            <h3 className="uppercase text-sm font-semibold tracking-widest textPrimary pt60 mt40 hiddenScrollAnimation">
              exploring patterns
            </h3>
            <div className="mt-0 h-[500px] md:h-[1100px] lg:h-[1100px]  relative">
              <Image
                src="/images/exploring-1.png"
                alt="lo fi wireframes image"
                fill
                style={{ objectFit: 'cover', objectPosition: 'top' }}
              />
            </div>
          </div>
        </div>

        <div className="lightBg text-black pt60 pb60">
          <SectionIntro
            containerStyleClass="pb-2.5"
            text="typography"
            bg="light"
          />
          <div className="grid-main-container">
            <div className="grid-container ">
              <ul className="list-disc caseStudyContent">
                <li className="hiddenScrollAnimation">
                  Variations of color palette and also possible variation of
                  typeface/typography were explored. We made sure to come up
                  with the best possible designs which aligned with our clients
                  as well as which would provide the best experience to our
                  users.
                </li>
              </ul>
              <div className="grid grid-cols-1 md:grid-cols-2 lg:grid-cols-2 gap-5 md:gap-10 lg:gap-10">
                <div className="mt40 h-[200px] md:h-[400px] lg:h-[400px] relative">
                  <Image
                    src="/images/typography-1.png"
                    fill
                    alt="lo fi wireframes image"
                    style={{ objectFit: 'cover', objectPosition: 'top' }}
                  />
                </div>
                <div className="mt40 h-[200px] md:h-[400px] lg:h-[400px] relative">
                  <Image
                    src="/images/typography-2.png"
                    fill
                    alt="lo fi wireframes image"
                    style={{ objectFit: 'cover', objectPosition: 'top' }}
                  />
                </div>
              </div>
              <div className="mt40 grid grid-cols-1 md:grid-cols-2 lg:grid-cols-2 gap5 md:gap-10 lg:gap-10 p20 bgGreen fontMontserrat">
                <div className="md:text-right lg:text-right">
                  <p className="f32 hiddenScrollAnimation">Montserrat</p>
                  <p className="mt10 hiddenScrollAnimation">Arimo</p>
                </div>
                <div>
                  <Title title="The TechnoServe Engine to Accelerate Entrepreneurs" />
                  <p className="f16 mt10 hiddenScrollAnimation">
                    TechnoServe harnesses entrepreneurship to tackle climate
                    change & poverty across 30 countries. We empower founders
                    behind word-class businesses by linking them to information
                  </p>
                </div>
              </div>
            </div>
          </div>
        </div>

        <SectionIntro
          containerStyleClass="mt60"
          text="final responsive ui designs"
        />
        <div className="grid-main-container mt40">
          <div className="grid-container">
            <p className="uppercase text-sm font-semibold tracking-widest textPrimary hiddenScrollAnimation">
              Landing home page
            </p>
            <div className="mt-5 grid grid-cols-1 md:grid-cols-2 lg:grid-cols-2 gap-5 md:gap-10 lg:gap-10">
              <div>
                <div className="h-[1415px] md:h-[2830px] lg:h-[2830px] relative">
                  <Image
                    src="/images/landing-home-page.png"
                    fill
                    alt="lo fi wireframes image"
                    style={{ objectFit: 'cover', objectPosition: 'top' }}
                  />
                </div>
              </div>
              <div>
                <div className="h-[250px] relative">
                  <Image
                    src="/images/sector.gif"
                    fill
                    alt="lo fi wireframes image"
                    style={{ objectFit: 'cover' }}
                  />
                </div>
                <p className="uppercase text-sm font-semibold tracking-widest textPrimary mt-5 hiddenScrollAnimation">
                  Mobile and tab view
                </p>
                <div className="mt-2 h-[800px] relative">
                  <Image
                    src="/images/mobile-tab-view.png"
                    fill
                    alt="lo fi wireframes image"
                    style={{ objectFit: 'cover' }}
                  />
                </div>
                <LinkBox
                  containerStyleClass="mt-5"
                  url="https://www.getgreenr.org/"
                  text="link to live home page"
                />
              </div>
            </div>
            <p className="mt60 pt40 uppercase text-sm font-semibold tracking-widest textPrimary hiddenScrollAnimation">
              About us
            </p>
            <div className="mt-5 grid grid-cols-1 md:grid-cols-2 lg:grid-cols-2 gap-5 md:gap-0 lg:gap-0">
              <div>
                <div className="h-[300px] relative sm:mr-0 md:mr-5 lg:mr-5">
                  <Image
                    src="/images/about.gif"
                    fill
                    alt="lo fi wireframes image"
                    style={{ objectFit: 'cover', objectPosition: 'top' }}
                  />
                </div>
                <div className="mt-5 h-[500px] relative">
                  <Image
                    src="/images/about-img1.png"
                    fill
                    alt="lo fi wireframes image"
                    style={{ objectFit: 'cover', objectPosition: 'top' }}
                  />
                </div>
                <LinkBox
                  containerStyleClass="mt-5 sm:mr-0 md:mr-5 lg:mr-5"
                  url="https://www.getgreenr.org/about"
                  text="link to live about us page"
                />
              </div>
              <div>
                <div className="h-[750px] md:h-[1500px]  lg:h-[1500px]  relative">
                  <Image
                    src="/images/about-img2.png"
                    fill
                    alt="lo fi wireframes image"
                    style={{ objectFit: 'cover', objectPosition: 'top' }}
                  />
                </div>
              </div>
            </div>
            <p className="mt60 pt40 uppercase text-sm font-semibold tracking-widest textPrimary hiddenScrollAnimation">
              inside greenr
            </p>
            <div className="mt-5 grid grid-cols-1 md:grid-cols-2 lg:grid-cols-2 gap-5 md:gap-10 lg:gap-10">
              <div>
                <div className="h-[550] md:h-[1100px]  lg:h-[1100px] relative">
                  <Image
                    src="/images/inside-greenr-1-2.png"
                    fill
                    alt="lo fi wireframes image"
                    style={{ objectFit: 'cover', objectPosition: 'top' }}
                  />
                </div>
              </div>
              <div>
                <div className="h-[300px] relative">
                  <Image
                    src="/images/timeline.gif"
                    fill
                    alt="lo fi wireframes image"
                    style={{ objectFit: 'cover', objectPosition: 'top' }}
                  />
                </div>
                <div className="mt-5 h-[300px] relative">
                  <Image
                    src="/images/banner.gif"
                    fill
                    alt="lo fi wireframes image"
                    style={{ objectFit: 'cover', objectPosition: 'top' }}
                  />
                </div>
                <LinkBox
                  containerStyleClass="mt-5 sm:mr-0 md:mr-5 lg:mr-5"
                  url="https://www.getgreenr.org/insideGreenr"
                  text="link to live inside greenr page"
                />
              </div>
            </div>
            <p className="mt40 pt60 uppercase text-sm font-semibold tracking-widest textPrimary hiddenScrollAnimation">
              teams and protfolio
            </p>
            <div className="mt-5 grid grid-cols-1 md:grid-cols-2 lg:grid-cols-2 gap-5 md:gap-10 lg:gap-10">
              <div>
                <div className="h-[450px] md:h-[900px] lg:h-[900px] relative">
                  <Image
                    src="/images/team-1-new.png"
                    fill
                    alt="lo fi wireframes image"
                    style={{ objectFit: 'cover', objectPosition: 'top' }}
                  />
                </div>
                <div className="mt-5 h-[600px] md:h-[600px] lg:h-[1200px] relative">
                  <Image
                    src="/images/team-4.png"
                    fill
                    alt="lo fi wireframes image"
                    style={{ objectFit: 'cover', objectPosition: 'top' }}
                  />
                </div>
              </div>
              <div>
                <div className="h-[530px] md:h-[1060px] lg:h-[1060px] relative">
                  <Image
                    src="/images/team-2-new.png"
                    fill
                    alt="lo fi wireframes image"
                    style={{ objectFit: 'cover', objectPosition: 'top' }}
                  />
                </div>
                <div className="mt-5 h-[200px] md:h-[400px] lg:h-[400px] relative">
                  <Image
                    src="/images/team-3.png"
                    fill
                    alt="lo fi wireframes image"
                    style={{ objectFit: 'cover', objectPosition: 'top' }}
                  />
                </div>
              </div>
            </div>
          </div>
        </div>

        <SectionIntro containerStyleClass="mt60 pt40" text="development" />
        <div className="grid-main-container mt-2.5">
          <div className="grid-container">
            <div className="grid grid-cols-1 md:grid-cols-2 lg:grid-cols-2 gap-5 md:gap-10 lg:gap-10">
              <ul className="list-disc caseStudyContent">
                <li className="hiddenScrollAnimation">
                  Technologies used for implementing this project were next.js
                  and tailwind.css.
                </li>
              </ul>
              <ul className="list-disc caseStudyContent">
                <li className="hiddenScrollAnimation">
                  Due to the uniqueness of the components, I mainly ended up
                  creating my own custom components or building components on
                  top of tailwind's components.
                </li>
              </ul>
            </div>
            <div className="mt40 h-[350px] flex justify-center">
              <Image
                src="/images/dev-3.gif"
                width={800}
                height={350}
                alt="lo fi wireframes image"
                style={{ objectFit: 'cover' }}
              />
            </div>
            <div className="mt40 grid grid-cols-1 md:grid-cols-2 lg:grid-cols-2 gap-5 md:gap-10 lg:gap-10">
              <div className="h-[300px] flex justify-center">
                <Image
                  src="/images/code-new.gif"
                  width={800}
                  height={350}
                  alt="lo fi wireframes image"
                  style={{ objectFit: 'cover' }}
                />
              </div>
              <div className="h-[150px] md:h-[300px] lg:h-[300px]  flex justify-center">
                <Image
                  src="/images/dev-1.png"
                  width={800}
                  height={350}
                  style={{ objectFit: 'cover' }}
                  alt="lo fi wireframes image"
                />
              </div>
            </div>
            <div className="mt40 grid grid-cols-1 md:grid-cols-2 lg:grid-cols-2 gap-5 md:gap-10 lg:gap-10">
              <ul className="list-disc caseStudyContent">
                <li className="hiddenScrollAnimation">
                  The components were all built keeping the reusable factor in
                  mind hence, in many cases the variations of same components
                  were implemented. I like to name my reusable components as
                  "shared components".
                </li>
              </ul>
              <ul className="list-disc caseStudyContent">
                <li className="hiddenScrollAnimation">
                  It was quite a challenge to build the custom components as all
                  the components were uniquely designed. I made sure to write
                  customizable, scalable and reusable codes.
                </li>
              </ul>
            </div>
          </div>
          <div className="grid-container">
            <div className="mt40 widthWrap relative">
              <Image
                src="/images/animation.gif"
                fill
                alt="lo fi wireframes image"
                style={{ objectFit: 'cover', objectPosition: 'top' }}
              />
            </div>
            <p className="mt40 caseStudyContent pb60 mb40 hiddenScrollAnimation">
              Animation requirement was also really a fun experience for me, i
              was able to code custom animation and learnt a lot about how
              animations works.
            </p>
          </div>
        </div>

        <Disclaimer disclaimerContent={greenRDisclaimer} />
      </div>
    </>
  );
};

export default GreenRWebsite;
