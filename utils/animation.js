function instersectioOberserverCallback(entries) {
  entries.forEach(entry => {
    if (entry.isIntersecting) {
      entry.target.classList.add('showScrollAnimation');
      entry.target.classList.add('showHeadingAnimation');
    } else {
      entry.target.classList.remove('showScrollAnimation');
      entry.target.classList.remove('showHeadingAnimation');
    }
  });
}

function afterCallback(observer) {
  const hiddenElements = document.querySelectorAll('.hiddenScrollAnimation');
  hiddenElements.forEach(el => observer.observe(el));
}

const rootMargin = {
  rootMargin: '30px',
};

export default { instersectioOberserverCallback, afterCallback, rootMargin };
