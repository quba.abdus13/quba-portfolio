// lib/gtag.js
export const pageview = url => {
  window.gtag('config', 'G-XV9S69P8ET', {
    page_path: url,
  });
};
